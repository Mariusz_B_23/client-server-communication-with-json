package mailbox;

import message.Message;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InboxTest {


    private Inbox inbox;
    private Message message1;
    private Message message2;

    @BeforeEach
    void setUp() {
        inbox = new Inbox();
        message1 = new Message("21.10.2023 23:31", "user", "hello!");
        message2 = new Message("21.10.2023 23:41", "user", "how are you?");
        inbox.addMessageToList(message1);
        inbox.addMessageToList(message2);
    }

    @AfterEach
    void clear() {
        inbox.clearAllMessagesFromInbox();
    }

    @Test
    @DisplayName("Inbox should contain two messages after adding them")
    void inboxShouldContainTwoMessagesAfterAdding() {
        assertEquals(2, inbox.getReadMessages().size());
    }

    @Test
    @DisplayName("should return message from the selected index in the list")
    void shouldReturnMessageFromTheSelectedIndexInTheList() {
        String message = inbox.getSelectedMessage(1);
        assertEquals(message2.toString(), message);
    }

    @Test
    @DisplayName("Remove Message at Index 2 and Verify Inbox State")
    void testRemoveMessageAtIndex2AndVerifyInbox() {
        inbox.removeSelectedMessageFromInbox(2);
        assertEquals(1, inbox.getReadMessages().size());
        assertEquals(message1.toString(), inbox.getSelectedMessage(0));
        assertFalse(inbox.getReadMessages().isEmpty());
    }

    @Test
    @DisplayName("Clear All Messages from Inbox and Verify It's Empty")
    void testClearAllMessagesAndVerifyInboxIsEmpty() {
        inbox.clearAllMessagesFromInbox();
        assertEquals(0, inbox.getReadMessages().size());
        assertTrue(inbox.getReadMessages().isEmpty());
    }

}
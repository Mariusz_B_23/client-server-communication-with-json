package mailbox;

import message.Message;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UnreadBoxTest {

    private UnreadBox unreadBox;

    @BeforeEach
    void setUp() {
        unreadBox = new UnreadBox();
        Message message1 = new Message("21.10.2023 23:31", "user", "hello!");
        Message message2 = new Message("21.10.2023 23:41", "user", "how are you?");
        unreadBox.addUnreadMessageToBox(message1);
        unreadBox.addUnreadMessageToBox(message2);
    }

    @AfterEach
    void clear() {
        unreadBox.getUnreadMessages().clear();
    }

    @Test
    @DisplayName("Test shouldReturnTwoUnreadMessages")
    void shouldReturnTwoUnreadMessages() {
        assertEquals(2, unreadBox.getUnreadMessages().size());
    }



}
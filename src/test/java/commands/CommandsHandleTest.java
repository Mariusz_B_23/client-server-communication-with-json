package commands;

import json.JsonWriter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("CommandsHandle class tests")
class CommandsHandleTest {

    private CommandsHandle commandsHandle;
    private JsonWriter jsonWriter;


    @BeforeEach
    void setUp() {
        commandsHandle = new CommandsHandle();
        jsonWriter = new JsonWriter();
    }

    @Test
    @DisplayName("Should return stop command message")
    void shouldReturnStopCommandMessage() {
        String stopCommand = commandsHandle.createStopCommandMessage();

        String stopHeadline = "stop";
        String stopCommandMessage = "Stopping the operation of the server.";
        String expectedCommand = jsonWriter.createJsonStringCommand(stopHeadline, stopCommandMessage);

        assertEquals(expectedCommand, stopCommand);
    }

    @Test
    @DisplayName("should return wrong command message")
    void shouldReturnWrongCommandMessage() {
        String wrongCommand = commandsHandle.createWrongCommandMessage();

        String wrongMessageHeadline = "wrongCommand";
        String wrongCommandMessage = "Incorrect command.";
        String expectedCommand = jsonWriter.createJsonStringCommand(wrongMessageHeadline, wrongCommandMessage);

        assertEquals(expectedCommand, wrongCommand);
    }

    @Test
    @DisplayName("should return server uptime")
    void testGetServerUptimeString() {
        Instant serverStartTime = Instant.parse("2023-10-26T10:00:00Z");

        String result = commandsHandle.getServerUptimeString(serverStartTime);

        Duration duration = Duration.between(serverStartTime, Instant.now());
        long hours = duration.toHours();
        long minutes = duration.toMinutesPart();
        long seconds = duration.toSecondsPart();
        String expected = String.format("Server operating time is: %d hours, %d minutes, %d seconds.",
                hours, minutes, seconds);

        assertEquals(expected, result);
    }

}
package users.user;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static users.user.UserManagement.*;

@DisplayName("UserManagement class Tests")
class UserManagementTest {

    private UserManagement userManagement;
    private User user;
    private static final String LOGIN = "user";
    private static final String PASSWORD = "0paSS#word0";
    private static final String INCORRECT_LOGIN = "adminUser";
    private static final String INCORRECT_PASSWORD = "5Pa$sword5";

    @BeforeEach
    void setUp() {
        userManagement = new UserManagement();
        user = userManagement.createNewUser(LOGIN, PASSWORD);
    }

    @AfterEach
    void clear() {
        getUserList().clear();
    }

    @Test
    @DisplayName("Should create a new user with correct data")
    void shouldNewUserCreateCorrectly() {
        assertNotNull(user);
        assertEquals("user", user.getLogin());
        assertEquals("0paSS#word0", user.getPassword());
    }

    @Test
    @DisplayName("Should be a user automatically added to the list after it is created")
    void shouldBeAUserAutomaticallyAddedToTheListAfterOItIsCreated() {
        assertTrue(getUserList().contains(user));
        assertNotNull(getUserList());
    }

    @Test
    @DisplayName("Should confirm user login exist on the list")
    void shouldUserLoginExistOnTheList() {
        assertTrue(doesLoginExistAsUser(LOGIN));
        assertFalse(doesLoginExistAsUser(INCORRECT_LOGIN));
    }

    @Test
    @DisplayName("Should confirm correct admin logging data")
    void shouldAdminLoggingDataBeCorrect() {
        assertTrue(userLoggingIsCorrect(LOGIN, PASSWORD));
        assertFalse(userLoggingIsCorrect(INCORRECT_LOGIN, INCORRECT_PASSWORD));
    }

    @Test
    @DisplayName("Should return user if logging data is correct")
    void shouldReturnUserIfLoggingDataIsCorrect() {
        User retrievedUser = userManagement.getUserFromList(LOGIN, PASSWORD);
        assertSame(retrievedUser, user);
        assertEquals(retrievedUser.getLogin(), user.getLogin());
        assertNotEquals(INCORRECT_LOGIN, user.getLogin());
        assertEquals(retrievedUser.getPassword(), user.getPassword());
        assertNotEquals(INCORRECT_PASSWORD, user.getPassword());
        assertEquals(retrievedUser.getMailBox(), user.getMailBox());
    }

    @Test
    @DisplayName("Should return user if login is correct")
    void shouldReturnUserIfLoginIsCorrect() {
        assertEquals(getUserFromListByLogin(LOGIN), user);
        assertNotEquals(getUserFromListByLogin(INCORRECT_LOGIN), user);
    }

    @Test
    @DisplayName("Should be user password valid")
    void shouldBeUserPasswordValid() {
        assertTrue(isUserPasswordValid(user, PASSWORD));
        assertFalse(isUserPasswordValid(user, INCORRECT_PASSWORD));
    }

    @Test
    @DisplayName("Should remove user from list")
    void shouldRemoveUserFromList() {
        assertTrue(getUserList().contains(user));
        removeUserFromList(user);
        assertTrue(getUserList().isEmpty());
    }

    @Test
    @DisplayName("Should read all users from JSON file")
    void shouldReadAllUsersFromJsonFile() {
        readAllUsersFromJsonFile();
        assertNotNull(getUserList());
        assertFalse(getUserList().isEmpty());
    }

}
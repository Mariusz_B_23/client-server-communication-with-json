package users.admin;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static users.admin.AdminManagement.*;

@DisplayName("AdminManagement class Tests")
class AdminManagementTest {

    private AdminManagement adminManagement;
    private Admin admin;
    private static final String LOGIN = "admin";
    private static final String PASSWORD = "9paSS#word9";
    private static final String INCORRECT_LOGIN = "userAdmin";
    private static final String INCORRECT_PASSWORD = "1Pa$sword1";


    @BeforeEach
    void setUp() {
        adminManagement = new AdminManagement();
        admin = adminManagement.createNewAdmin(LOGIN, PASSWORD);
    }

    @AfterEach
    void clear() {
        getAdminList().clear();
    }

    @Test
    @DisplayName("Should create a new admin with correct data")
    void shouldNewAdminCreateCorrectly() {
        assertNotNull(admin);
        assertEquals("admin", admin.getLogin());
        assertEquals("9paSS#word9", admin.getPassword());
    }

    @Test
    @DisplayName("Should be a admin automatically added to the list after it is created")
    void shouldBeAdminAutomaticallyAddedToTheListAfterOItIsCreated() {
        assertTrue(getAdminList().contains(admin));
    }

    @Test
    @DisplayName("Should confirm admin login exist on the list")
    void shouldAdminLoginExistOnTheList() {
        assertTrue(doesLoginExistAsAdmin(LOGIN));
        assertFalse(doesLoginExistAsAdmin(INCORRECT_LOGIN));
    }

    @Test
    @DisplayName("Should confirm correct admin logging data")
    void shouldAdminLoggingDataBeCorrect() {
        assertTrue(adminLoggingIsCorrect(LOGIN, PASSWORD));
        assertFalse(adminLoggingIsCorrect(INCORRECT_LOGIN, INCORRECT_PASSWORD));
    }

    @Test
    @DisplayName("Should return admin if logging data is correct")
    void shouldReturnAdminIfLoggingDataIsCorrect() {
        Admin retrievedAdmin = adminManagement.getAdminFromListByPasswordAndLogin(LOGIN, PASSWORD);
        assertSame(retrievedAdmin, admin);
        assertEquals(retrievedAdmin.getLogin(), admin.getLogin());
        assertNotEquals(INCORRECT_LOGIN, admin.getLogin());
        assertEquals(retrievedAdmin.getPassword(), admin.getPassword());
        assertNotEquals(INCORRECT_PASSWORD, admin.getPassword());
        assertEquals(retrievedAdmin.getMailBox(), admin.getMailBox());
    }

    @Test
    @DisplayName("Should return admin if login is correct")
    void shouldReturnAdminIfLoginIsCorrect() {
        assertEquals(getAdminFromListByLogin(LOGIN), admin);
        assertNotEquals(getAdminFromListByLogin(INCORRECT_LOGIN), admin);
    }

    @Test
    @DisplayName("Should be admin password valid")
    void shouldBeAdminPasswordValid() {
        assertTrue(isAdminPasswordValid(admin, PASSWORD));
        assertFalse(isAdminPasswordValid(admin, INCORRECT_PASSWORD));
    }

    @Test
    @DisplayName("Should remove admin from list")
    void shouldRemoveAdminFromList() {
        assertTrue(getAdminList().contains(admin));
        removeAdminFromList(admin);
        assertTrue(getAdminList().isEmpty());
    }

    @Test
    @DisplayName("Should read all admins from JSON file")
    void shouldReadAllAdminsFromJsonFile() {
        readAllAdminsFromJsonFile();
        assertNotNull(getAdminList());
        assertFalse(getAdminList().isEmpty());
    }

}
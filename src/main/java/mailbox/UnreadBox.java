package mailbox;


import lombok.Getter;
import message.Message;
import users.admin.Admin;
import users.admin.AdminManagement;
import users.user.User;
import users.user.UserManagement;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static validation.ValidationService.isMessageValid;
import static validation.ValidationService.isStringValid;

@Getter
public class UnreadBox {

    private final List<Message> unreadMessages;
    private static final int MAXIMUM_SIZE_OF_LIST = 5;

    public UnreadBox() {
        unreadMessages = new ArrayList<>();
    }

    public String getMessage(int index) {
        return unreadMessages.get(index).toString();
    }

    public static int getMaximumSizeOfList() {
        return MAXIMUM_SIZE_OF_LIST;
    }

    public void clearAllMessagesFromUnreadbox() {
        unreadMessages.clear();
    }

    public void addUnreadMessageToRecipientUnreadBox(String recipient, Message message) {
        isStringValid(recipient);
        isMessageValid(message);
        if (UserManagement.doesLoginExistAsUser(recipient)) {
            for (User user : UserManagement.getUserList()) {
                if (user.getLogin().equals(recipient)) {
                    user.getMailBox().getUnreadBox().addUnreadMessageToBox(message);
                }
            }
        } else if (AdminManagement.doesLoginExistAsAdmin(recipient)) {
            for (Admin admin : AdminManagement.getAdminList()) {
                if (admin.getLogin().equals(recipient)) {
                    admin.getMailBox().getUnreadBox().addUnreadMessageToBox(message);
                }
            }
        }
    }

    public void addUnreadMessageToBox(Message message) {
        unreadMessages.add(message);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("--- UnreadBox ---").append(System.lineSeparator());
        if (unreadMessages.isEmpty()) {
            stringBuilder.append("There are no messages in the unread mailbox.");
        } else {
            for (int i = 0; i < unreadMessages.size(); i++) {
                stringBuilder.append("- Message nr ").append((i + 1));
                stringBuilder.append(unreadMessages.get(i)).append(System.lineSeparator());
            }
        }
        return stringBuilder.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnreadBox unreadBox = (UnreadBox) o;
        return Objects.equals(unreadMessages, unreadBox.unreadMessages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(unreadMessages);
    }
}
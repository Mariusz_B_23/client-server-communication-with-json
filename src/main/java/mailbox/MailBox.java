package mailbox;

import lombok.Getter;

import java.util.Objects;

@Getter
public class MailBox {

    private final Inbox inbox;
    private final UnreadBox unreadBox;


    public MailBox() {
        inbox = new Inbox();
        unreadBox = new UnreadBox();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MailBox mailBox = (MailBox) o;
        return Objects.equals(inbox, mailBox.inbox) && Objects.equals(unreadBox, mailBox.unreadBox);
    }

    @Override
    public int hashCode() {
        return Objects.hash(inbox, unreadBox);
    }

    @Override
    public String toString() {
        return inbox + System.lineSeparator()
                + unreadBox + System.lineSeparator();
    }

}
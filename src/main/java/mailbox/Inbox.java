package mailbox;

import lombok.Data;
import message.Message;

import java.util.ArrayList;
import java.util.List;

@Data
public class Inbox {

    private final List<Message> readMessages;

    public Inbox() {
        readMessages = new ArrayList<>();
    }

    public String getSelectedMessage(int index) {
        return readMessages.get(index).toString();
    }

    public Message getMessage(int index) {
        return readMessages.get(index);
    }

    public void addMessageToList(Message message) {
        readMessages.add(message);
    }

    public void removeSelectedMessageFromInbox(int index) {
        readMessages.remove((index - 1));
    }

    public void clearAllMessagesFromInbox() {
        readMessages.clear();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("--- Inbox ---").append(System.lineSeparator());
        if (readMessages.isEmpty()) {
            stringBuilder.append("There are no messages in the Inbox.");
        } else {
            for (int i = 0; i < readMessages.size(); i++) {
                stringBuilder.append("- Message nr ").append((i + 1));
                stringBuilder.append(readMessages.get(i)).append(System.lineSeparator());
            }
        }
        return stringBuilder.toString();
    }

}
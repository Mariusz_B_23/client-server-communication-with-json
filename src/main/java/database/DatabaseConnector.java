package database;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnector {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");
    private static final String URL = "jdbc:postgresql://localhost:5432/cs";
    private static final String USER = "zr";
    private static final String PASSWORD = "zr";

    private DatabaseConnector() {
    }

    public static Connection connect() {
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
            LOGGER.info("Connected to the database.");
        } catch (SQLException e) {
            LOGGER.error("Error connecting to the database: {}", e.getMessage(), e);
        }
        return connection;
    }

}
package database;

import exceptions.CodeGenerationException;
import exceptions.DSLContextCreationException;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.codegen.GenerationTool;
import org.jooq.impl.DSL;
import org.jooq.meta.jaxb.*;

import java.sql.Connection;

public class JOOQConfigurationInitializer {


    public JOOQConfigurationInitializer() {
    }

    public DSLContext createDSLContext(Connection connection) {
        try {
            return DSL.using(connection, SQLDialect.POSTGRES);
        } catch (Exception e) {
            throw new DSLContextCreationException("Failed to create a DSL context.", e);
        }
    }

    public void initializeAndGenerate() {
        try {
            Configuration configuration = createConfiguration();
            GenerationTool.generate(configuration);
        } catch (Exception e) {
            throw new CodeGenerationException("Failed to generate code.", e);
        }
    }

    private Configuration createConfiguration() {
        return new Configuration()
                .withJdbc(new Jdbc()
                        .withDriver("org.postgresql.Driver")
                        .withUrl("jdbc:postgresql://localhost:5432/cs")
                        .withUser("zr")
                        .withPassword("zr"))
                .withGenerator(new Generator()
                        .withName("org.jooq.codegen.JavaGenerator")
                        .withDatabase(new Database()
                                .withName("org.jooq.meta.postgres.PostgresDatabase")
                                .withIncludes(".*")
                                .withInputSchema("public"))
                        .withTarget(new Target()
                                .withPackageName("jooq.generate")
                                .withDirectory("src/main/java")));
    }

}
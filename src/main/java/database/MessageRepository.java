package database;

import jooq.generate.Tables;
import jooq.generate.tables.records.MessageRecord;
import message.Message;
import org.jooq.DSLContext;
import users.BaseUser;

import static jooq.generate.Tables.MESSAGE;
import static org.jooq.impl.DSL.*;

public class MessageRepository {

    private final DSLContext create;

    public MessageRepository(DSLContext create) {
        this.create = create;
    }

    public void createMessage(Message message) {
        MessageRecord messageRecord = create.newRecord(MESSAGE);
        messageRecord.setUserId(determineTheUserIdFromTheLogin(message.getRecipient()));
        messageRecord.setIsRead(message.getIsRead());
        messageRecord.setReceived(message.getReceivedDate());
        messageRecord.setSenderId(determineTheUserIdFromTheLogin(message.getSender()));
        messageRecord.setContent(message.getContent());
        messageRecord.store();
    }

    private Long determineTheUserIdFromTheLogin(String login) {
        return create.select(field("id"))
                .from(table("users"))
                .where(field("login").eq(login))
                .fetchOneInto(Long.class);
    }

    public void changeTheReadingStatus(BaseUser baseUser) {
        create.update(Tables.MESSAGE)
                .set(Tables.MESSAGE.IS_READ, true)
                .from(Tables.USERS)
                .where(Tables.USERS.LOGIN.eq(baseUser.getLogin()))
                .and(Tables.USERS.ID.eq(MESSAGE.USER_ID))
                .execute();
    }

    public void deleteSingleMessage(Message message) {
        create.deleteFrom(MESSAGE)
                .where(MESSAGE.IS_READ.eq(message.getIsRead()))
                .and(MESSAGE.RECEIVED.eq(message.getReceivedDate()))
                .and(MESSAGE.CONTENT.eq(message.getContent()))
                .execute();
    }

    public void deleteAllMessages(BaseUser baseUser) {
        create.deleteFrom(Tables.MESSAGE)
                .where(Tables.MESSAGE.USER_ID.in(
                        select(Tables.USERS.ID)
                                .from(Tables.USERS)
                                .where(Tables.USERS.LOGIN.eq(baseUser.getLogin()))
                ))
                .execute();
    }

}
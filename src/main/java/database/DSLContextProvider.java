package database;

import org.jooq.DSLContext;

import java.sql.Connection;

public class DSLContextProvider {

    private DSLContextProvider() {
    }

    public static DSLContext createDslContext() {
        Connection connection = DatabaseConnector.connect();
        JOOQConfigurationInitializer initializer = new JOOQConfigurationInitializer();
        return initializer.createDSLContext(connection);
    }

}
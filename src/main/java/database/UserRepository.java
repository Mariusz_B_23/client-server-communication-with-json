package database;

import jooq.generate.Tables;
import jooq.generate.enums.HandlingRights;
import jooq.generate.tables.records.UsersRecord;
import org.jooq.DSLContext;
import users.BaseUser;
import users.admin.Admin;
import users.user.User;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

public class UserRepository {

    private final DSLContext create;
    private static final String LOGIN = "login";
    private static final String PASSWORD = "password";
    private static final String USERS = "users";
    private static final String IS_ADMIN = "is_admin";
    private static final String HANDLING_RIGHTS = "handling_rights";

    public UserRepository(DSLContext create) {
        this.create = create;
    }

    public void createUser(BaseUser user) {
        UsersRecord usersRecord = create.newRecord(Tables.USERS);
        usersRecord.setLogin(user.getLogin());
        usersRecord.setPassword(user.getPassword());
        usersRecord.setHandlingRights(convertToJooqEnum(user.getHandlingRights()));
        usersRecord.setIsadmin(user.getIsAdmin());
        usersRecord.store();
    }

    private jooq.generate.enums.HandlingRights convertToJooqEnum(utils.HandlingRights handlingRights) {
        return switch (handlingRights) {
            case USER -> HandlingRights.USER;
            case ADMIN -> HandlingRights.ADMIN;
        };
    }

    public boolean checkingIfAUserLoginExists(String login) {
        return create.fetchExists(
                create.selectOne()
                        .from(table(USERS))
                        .where(field(LOGIN).eq(login))
        );
    }

    public void changeRoleToAdmin(User user) {
        create.update(table(USERS))
                .set(field(IS_ADMIN), true)
                .set(field(HANDLING_RIGHTS), HandlingRights.ADMIN)
                .where(field(LOGIN).eq(user.getLogin()))
                .execute();
    }

    public void changeRoleToUser(Admin admin) {
        create.update(table(USERS))
                .set(field(IS_ADMIN), false)
                .set(field(HANDLING_RIGHTS), HandlingRights.USER)
                .where(field(LOGIN).eq(admin.getLogin()))
                .execute();
    }

    public void changePassword(BaseUser baseUser, String newPassword) {
        create.update(table(USERS))
                .set(field(PASSWORD), newPassword)
                .where(field(LOGIN).eq(baseUser.getLogin()))
                .execute();
    }

    public void deleteUserByLogin(String login) {
        create.deleteFrom(table(USERS))
                .where(field(LOGIN).eq(login))
                .execute();
    }

}
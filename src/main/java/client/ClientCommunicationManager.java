package client;

import client.account.logging.AccountLoginHandle;
import client.account.registration.NewAccountRegistrationHandle;
import commands.CommandsHandle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.CommonUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.Socket;
import java.util.Scanner;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedReaderUtils.showIncomingMessage;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class ClientCommunicationManager {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void initiationOfCommunicationWithTheServer(Socket clientSocket, BufferedWriter bufferedWriter,
                                                       BufferedReader bufferedReader, Scanner scanner) {
        isClientSocketValid(clientSocket);
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isScannerValid(scanner);
        CommonUtils commonUtils = new CommonUtils();
        commonUtils.receiveInitialInstructionsFromServer(bufferedReader);
        String userSelectedCommand;
        while (!clientSocket.isClosed()) {
            String adminListStatus =
                    returnTheAnswerAsString(bufferedReader);
            boolean hasAdministratorAccount =
                    Boolean.parseBoolean(adminListStatus);
            userSelectedCommand = getUserInput(scanner).toLowerCase();
            sendMessage(bufferedWriter, userSelectedCommand + System.lineSeparator());
            if (userSelectedCommand.equals("stop")) {
                CommandsHandle commandsHandle = new CommandsHandle();
                commandsHandle.processStopCommandForClientSocket(bufferedReader, clientSocket);
            } else {
                switch (userSelectedCommand) {
                    case "1" -> {
                        NewAccountRegistrationHandle newAccountRegistrationHandle = new NewAccountRegistrationHandle();
                        newAccountRegistrationHandle.handleClientRegistration
                                (bufferedWriter, bufferedReader, scanner, hasAdministratorAccount);
                    }
                    case "2" -> {
                        AccountLoginHandle accountLoginHandle = new AccountLoginHandle();
                        accountLoginHandle.handlingClientLogging
                                (bufferedWriter, bufferedReader, scanner, hasAdministratorAccount);
                    }
                    default -> showIncomingMessage(bufferedReader);
                }
                commonUtils.receiveInitialInstructionsFromServer(bufferedReader);
            }
        }
    }

    private String getUserInput(Scanner scanner) {
        String userInput;
        do {
            userInput = scanner.nextLine();
            if (userInput.isEmpty()) {
                LOGGER.info("The input cannot be empty.\n\nPlease enter again:\n");

            }
        } while (userInput.isEmpty());
        return userInput;
    }

}
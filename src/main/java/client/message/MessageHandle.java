package client.message;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.CommonMethods;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Scanner;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedReaderUtils.showIncomingMessage;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class MessageHandle {

    private final CommonMethods commonMethods = new CommonMethods();
    private static final Logger logger = LogManager.getLogger("cs_app");

    public void processMessageCommands
            (BufferedReader bufferedReader, String messageCommand, Scanner scanner, BufferedWriter bufferedWriter) {
        isBufferedReaderValid(bufferedReader);
        isStringValid(messageCommand);
        isScannerValid(scanner);
        isBufferedWriterValid(bufferedWriter);
        switch (messageCommand.trim()) {
            case "1" -> {
                showIncomingMessage(bufferedReader);
                boolean isTheUserInTheDatabase;
                do {
                    String userProvidesNickname = commonMethods.getUserInputNonEmpty(scanner) + System.lineSeparator();
                    sendMessage(bufferedWriter, userProvidesNickname);
                    String isTheUserInTheDatabaseAsString = returnTheAnswerAsString(bufferedReader);
                    isTheUserInTheDatabase = Boolean.parseBoolean(isTheUserInTheDatabaseAsString);
                    if (!isTheUserInTheDatabase) {
                        showIncomingMessage(bufferedReader);
                        showIncomingMessage(bufferedReader);
                    }
                } while (!isTheUserInTheDatabase);
                String isUserUnreadMessagesBoxFullAsString = returnTheAnswerAsString(bufferedReader);
                boolean isUserUnreadMessagesBoxFull = Boolean.parseBoolean(isUserUnreadMessagesBoxFullAsString);
                if (isUserUnreadMessagesBoxFull) {
                    showIncomingMessage(bufferedReader);
                } else {
                    String typeMessageHeadline = returnTheAnswerAsString(bufferedReader);
                    logger.info("\n{}", typeMessageHeadline);
                    boolean isMessageTooLong;
                    String messageByUser;
                    do {
                        messageByUser = commonMethods.getUserInputNonEmpty(scanner) + System.lineSeparator();
                        sendMessage(bufferedWriter, messageByUser);
                        String isMessageTooLongAsString = returnTheAnswerAsString(bufferedReader);
                        isMessageTooLong = Boolean.parseBoolean(isMessageTooLongAsString);
                        if (isMessageTooLong) {
                            showIncomingMessage(bufferedReader);
                            showIncomingMessage(bufferedReader);
                        }
                    } while (isMessageTooLong);
                    showIncomingMessage(bufferedReader);
                }
            }
            case "2", "3" -> printMessages(bufferedReader);
            case "4" -> {
                String inboxIsEmpty = returnTheAnswerAsString(bufferedReader);
                boolean isInboxIsEmpty = Boolean.parseBoolean(inboxIsEmpty);
                if (isInboxIsEmpty) {
                    showIncomingMessage(bufferedReader);
                } else {
                    String selectDeleteOptionHeadline = returnTheAnswerAsString(bufferedReader);
                    logger.info("\n{}", selectDeleteOptionHeadline);
                    String userChooseOption = theUserSelectsTheTypeOfDeletion(scanner) + System.lineSeparator();
                    sendMessage(bufferedWriter, userChooseOption);
                    if (userChooseOption.trim().equals("s")) {
                        boolean correctMessageIndex;
                        do {
                            showIncomingMessage(bufferedReader);

                            String indexOfMessage = theUserSelectsNumberOfMessageToDelete(scanner)
                                    + System.lineSeparator();
                            sendMessage(bufferedWriter, indexOfMessage);

                            String correctMessageIndexAsString = returnTheAnswerAsString(bufferedReader);
                            correctMessageIndex = Boolean.parseBoolean(correctMessageIndexAsString);
                            showIncomingMessage(bufferedReader);
                        } while (!correctMessageIndex);
                    } else if (userChooseOption.trim().equals("a")) {
                        showIncomingMessage(bufferedReader);
                        String certification = "message";
                        String userChooseYesOrNo = commonMethods.getUserConfirmationForDeletion(scanner, certification) + System.lineSeparator();
                        sendMessage(bufferedWriter, userChooseYesOrNo);
                        showIncomingMessage(bufferedReader);
                    }
                }
            }
            default -> showIncomingMessage(bufferedReader);
        }
    }

    private void printMessages(BufferedReader bufferedReader) {
        isBufferedReaderValid(bufferedReader);
        String numberOfRreadMessagesAsString = returnTheAnswerAsString(bufferedReader);
        int numberOfReadMessagesAsInteger = Integer.parseInt(numberOfRreadMessagesAsString);
        String readHeadline = returnTheAnswerAsString(bufferedReader);
        logger.info("\n{}", readHeadline);
        if (numberOfReadMessagesAsInteger > 0) {
            for (int i = 0; i < numberOfReadMessagesAsInteger; i++) {
                showIncomingMessage(bufferedReader);
            }
        }
    }

    private String theUserSelectsTheTypeOfDeletion(Scanner scanner) {
        isScannerValid(scanner);
        String regex = "^([sa])$";
        String input;
        do {
            input = scanner.nextLine();
            if (!input.matches(regex)) {
                logger.info("You must choose option s (single) or a (all).");
            }
        } while (!input.matches(regex));
        return input;
    }

    private String theUserSelectsNumberOfMessageToDelete(Scanner scanner) {
        isScannerValid(scanner);
        String regex = "^[1-9]\\d{0,5}$";
        String input;
        do {
            input = scanner.nextLine();
            if (!input.matches(regex)) {
                logger.info("Enter the correct number greater than 0.");
            }
        } while (!input.matches(regex));
        return input;
    }

}
package client.menu;

import client.account.deletion.AccountDeletionHandle;
import client.account.managing.ChangePasswordHandle;
import client.account.managing.ManagingOthersAccountHandle;
import utils.CommonMethods;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Scanner;

import static utils.BufferedReaderUtils.showIncomingMessage;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class AdminMenu {

    private final CommonMethods commonMethods = new CommonMethods();

    public void adminMenuHandle
            (BufferedWriter bufferedWriter, BufferedReader bufferedReader, Scanner scanner, String login) {
        isBufferedReaderValid(bufferedReader);
        isBufferedWriterValid(bufferedWriter);
        isScannerValid(scanner);
        isStringValid(login);
        boolean logout = false;
        String userChooseOption;
        do {
            showIncomingMessage(bufferedReader);
            userChooseOption = commonMethods.getUserInputNonEmpty(scanner);
            sendMessage(bufferedWriter, userChooseOption + System.lineSeparator());
            if (userChooseOption.equals("6")) {
                logout = true;
                showIncomingMessage(bufferedReader);
            } else if (userChooseOption.equals("4")) {
                AccountDeletionHandle accountDeletionHandle = new AccountDeletionHandle();
                logout = accountDeletionHandle.confirmDeletion(bufferedWriter, bufferedReader, scanner);
            } else {
                switch (userChooseOption) {
                    case "1" -> commonMethods.handleOptionCommandsUntilBack(bufferedReader, scanner, bufferedWriter);
                    case "2" -> commonMethods.handleMessagesUntilBack(bufferedReader, scanner, bufferedWriter);
                    case "3" -> {
                        ChangePasswordHandle changePasswordHandle = new ChangePasswordHandle();
                        changePasswordHandle.changePasswordWithVerification(bufferedReader, scanner, bufferedWriter);
                    }
                    case "5" -> {
                        ManagingOthersAccountHandle managingOthersAccountHandle = new ManagingOthersAccountHandle();
                        managingOthersAccountHandle.chooseOptionHandle(bufferedWriter, bufferedReader, scanner);
                    }
                    default -> showIncomingMessage(bufferedReader);
                }
            }
        } while (!logout);
    }

}
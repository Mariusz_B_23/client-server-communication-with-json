package client.account.deletion;

import utils.CommonMethods;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Scanner;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedReaderUtils.showIncomingMessage;
import static utils.BufferedWriterUtils.sendMessage;
import static utils.CommonUtils.getUserInputCertification;
import static validation.ValidationService.*;

public class AccountDeletionHandle {


    public boolean confirmDeletion
            (BufferedWriter bufferedWriter, BufferedReader bufferedReader, Scanner scanner) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isScannerValid(scanner);
        CommonMethods commonMethods = new CommonMethods();
        showIncomingMessage(bufferedReader);
        String accountCertification = "account";
        String clientConfirm =
                commonMethods.getUserConfirmationForDeletion(scanner, accountCertification) + System.lineSeparator();
        sendMessage(bufferedWriter, clientConfirm);
        boolean clientConfirmAsBoolean = clientConfirm.trim().equals("y");
        if (clientConfirmAsBoolean) {
            handleAccountDeletion(bufferedWriter, bufferedReader, scanner);
        }
        return clientConfirmAsBoolean;
    }

    private void handleAccountDeletion(BufferedWriter bufferedWriter, BufferedReader bufferedReader, Scanner scanner) {
        showIncomingMessage(bufferedReader);
        boolean passwordIsCorrect;
        do {
            passwordIsCorrect = performAccountDeletion(bufferedWriter, bufferedReader, scanner);
        } while (!passwordIsCorrect);
    }

    public boolean performAccountDeletion
            (BufferedWriter bufferedWriter, BufferedReader bufferedReader, Scanner scanner) {
        boolean confirmationOfCorrectPassword;
        String passwordToConfirm;
        String passwordCertification = "password";
        int minimumLength = 8;
        passwordToConfirm = getUserInputCertification(scanner, passwordCertification, minimumLength)
                + System.lineSeparator();
        sendMessage(bufferedWriter, passwordToConfirm);
        confirmationOfCorrectPassword = Boolean.parseBoolean(returnTheAnswerAsString(bufferedReader));
        showIncomingMessage(bufferedReader);
        return confirmationOfCorrectPassword;
    }

}
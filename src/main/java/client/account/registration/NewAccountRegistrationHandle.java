package client.account.registration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Scanner;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedReaderUtils.showIncomingMessage;
import static utils.BufferedWriterUtils.sendMessage;
import static utils.CommonUtils.getUserInputCertification;
import static validation.ValidationService.*;

public class NewAccountRegistrationHandle {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void handleClientRegistration(BufferedWriter bufferedWriter, BufferedReader bufferedReader,
                                         Scanner scanner, boolean hasAdministratorAccount) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isScannerValid(scanner);
        if (!hasAdministratorAccount) {
            handleFirstAdminRegistration(bufferedWriter, bufferedReader, scanner);
        } else {
            handleUserRegistration(bufferedReader, bufferedWriter, scanner);
        }
    }

    private void handleFirstAdminRegistration
            (BufferedWriter bufferedWriter, BufferedReader bufferedReader, Scanner scanner) {

        showIncomingMessage(bufferedReader);
        String adminLogin = getUniqueLogin(bufferedReader, bufferedWriter, scanner);
        showIncomingMessage(bufferedReader);
        String adminPassword = getInputPassword(scanner);
        sendMessage(bufferedWriter, adminPassword);
        notificationThatAFirstAdministratorHasBeenCorrectlyRegistered(bufferedReader, adminLogin);
    }

    public void handleUserRegistration
            (BufferedReader bufferedReader, BufferedWriter bufferedWriter, Scanner scanner) {
        LOGGER.info("\nUSER ACCOUNT REGISTRATION");
        showIncomingMessage(bufferedReader);
        String userLogin = getUniqueLogin(bufferedReader, bufferedWriter, scanner);
        showIncomingMessage(bufferedReader);
        String userPassword = getInputPassword(scanner);
        sendMessage(bufferedWriter, userPassword);
        notificationThatANewUserHasBeenCorrectlyRegistered(bufferedReader, userLogin);
    }

    private String getUniqueLogin(BufferedReader bufferedReader, BufferedWriter bufferedWriter, Scanner scanner) {
        String validateLoginOnServerSide;
        String userLogin;
        boolean loginExists;
        do {
            String certification = "login";
            int minimumLoginLength = 3;
            userLogin = getUserInputCertification(scanner, certification, minimumLoginLength) + System.lineSeparator();
            sendMessage(bufferedWriter, userLogin);
            validateLoginOnServerSide = returnTheAnswerAsString(bufferedReader);
            loginExists = Boolean.parseBoolean(validateLoginOnServerSide);
            if (loginExists) {
                LOGGER.info("\nThe specified login exists in the database.\n\nEnter a new login:");
            } else {
                LOGGER.info("\nThe correct login was provided.");
            }
        } while (loginExists);
        return userLogin;
    }

    private String getInputPassword(Scanner scanner) {
        String certification = "password";
        int minimumPasswordLength = 8;
        return getUserInputCertification(scanner, certification, minimumPasswordLength) + System.lineSeparator();
    }

    private void notificationThatANewUserHasBeenCorrectlyRegistered
            (BufferedReader bufferedReader, String userLogin) {
        showIncomingMessage(bufferedReader);
        LOGGER.info("\nUser \"{}\" was created.\n", userLogin.trim());

    }

    private void notificationThatAFirstAdministratorHasBeenCorrectlyRegistered
            (BufferedReader bufferedReader, String adminLogin) {
        showIncomingMessage(bufferedReader);
        LOGGER.info("\nAdmin \"{}\" was created.\n", adminLogin.trim());
    }

}
package client.account.managing;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Scanner;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedReaderUtils.showIncomingMessage;
import static utils.BufferedWriterUtils.sendMessage;
import static utils.CommonUtils.getUserInputCertification;
import static validation.ValidationService.isBufferedReaderValid;
import static validation.ValidationService.isScannerValid;

public class ChangePasswordHandle {

    public static final int MINIMUM_PASSWORD_LENGTH = 8;

    public void changePasswordWithVerification
            (BufferedReader bufferedReader, Scanner scanner, BufferedWriter bufferedWriter) {
        isBufferedReaderValid(bufferedReader);
        isScannerValid(scanner);
        isBufferedReaderValid(bufferedReader);
        showIncomingMessage(bufferedReader);
        String certification = "password";
        String oldPassword = getUserInputCertification(scanner, certification, MINIMUM_PASSWORD_LENGTH)
                + System.lineSeparator();
        sendMessage(bufferedWriter, oldPassword);
        String passwordValid = returnTheAnswerAsString(bufferedReader);
        boolean passwordValidAsBoolean = Boolean.parseBoolean(passwordValid);
        showIncomingMessage(bufferedReader);
        if (passwordValidAsBoolean) {
            String newPassword;
            String newPasswordEnterAgain;
            boolean confirmationOfCorrectNewPassword;
            do {
                showIncomingMessage(bufferedReader);
                newPassword = getUserInputCertification(scanner, certification, MINIMUM_PASSWORD_LENGTH)
                        + System.lineSeparator();
                sendMessage(bufferedWriter, newPassword);
                showIncomingMessage(bufferedReader);
                newPasswordEnterAgain =
                        getUserInputCertification(scanner, certification, MINIMUM_PASSWORD_LENGTH)
                                + System.lineSeparator();
                sendMessage(bufferedWriter, newPasswordEnterAgain);
                String passwordVerification = returnTheAnswerAsString(bufferedReader);
                confirmationOfCorrectNewPassword = Boolean.parseBoolean(passwordVerification);
                showIncomingMessage(bufferedReader);
            } while (!confirmationOfCorrectNewPassword);
        }
    }

}
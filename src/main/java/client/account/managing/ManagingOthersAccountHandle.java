package client.account.managing;

import utils.CommonMethods;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Scanner;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedReaderUtils.showIncomingMessage;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class ManagingOthersAccountHandle {


    public void chooseOptionHandle(BufferedWriter bufferedWriter, BufferedReader bufferedReader,
                                   Scanner scanner) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isScannerValid(scanner);
        showIncomingMessage(bufferedReader);
        CommonMethods commonMethods = new CommonMethods();
        boolean adminChooseOptionBack = false;
        do {
            String optionSelectedByAdmin = commonMethods.getUserInputNonEmpty(scanner) + System.lineSeparator();
            sendMessage(bufferedWriter, optionSelectedByAdmin);
            switch (optionSelectedByAdmin.trim()) {
                case "1" -> {
                    String isUserListEmptyAsString = returnTheAnswerAsString(bufferedReader);
                    boolean isUserListEmpty = Boolean.parseBoolean(isUserListEmptyAsString);
                    if (isUserListEmpty) {
                        showIncomingMessage(bufferedReader);
                    } else {
                        commonMethods.handleUserInputAndValidation(bufferedWriter, scanner, bufferedReader);
                    }
                }
                case "2" -> {
                    String isOnlyOnaAdminAccountAsString = returnTheAnswerAsString(bufferedReader);
                    boolean isOnlyOnaAdminAccount = Boolean.parseBoolean(isOnlyOnaAdminAccountAsString);
                    if (isOnlyOnaAdminAccount) {
                        showIncomingMessage(bufferedReader);
                    } else {
                        commonMethods.handleUserInputAndValidation(bufferedWriter, scanner, bufferedReader);
                    }
                }
                case "3" -> {
                    String isOnlyOneRegisteredAccountAsString = returnTheAnswerAsString(bufferedReader);
                    boolean isOnlyOneRegisteredAccount = Boolean.parseBoolean(isOnlyOneRegisteredAccountAsString);
                    if (isOnlyOneRegisteredAccount) {
                        showIncomingMessage(bufferedReader);
                    } else {
                        commonMethods.handleUserInputAndValidation(bufferedWriter, scanner, bufferedReader);
                    }
                }
                case "back" -> {
                    adminChooseOptionBack = true;
                    showIncomingMessage(bufferedReader);
                }
                default -> showIncomingMessage(bufferedReader);
            }
        } while (!adminChooseOptionBack);
    }

}
package client.account.logging;

import client.menu.AdminMenu;
import client.menu.UserMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.HandlingRights;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Scanner;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedReaderUtils.showIncomingMessage;
import static utils.BufferedWriterUtils.sendMessage;
import static utils.CommonUtils.getUserInputCertification;
import static validation.ValidationService.*;

public class AccountLoginHandle {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void handlingClientLogging(BufferedWriter bufferedWriter, BufferedReader bufferedReader,
                                      Scanner scanner, boolean hasAtLeastOneAdministratorAccount) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isScannerValid(scanner);
        if (!hasAtLeastOneAdministratorAccount) {
            showIncomingMessage(bufferedReader);
        } else {
            processClientAuthentication(bufferedReader, bufferedWriter, scanner);
        }
    }

    private void processClientAuthentication
            (BufferedReader bufferedReader, BufferedWriter bufferedWriter, Scanner scanner) {
        LOGGER.info("\nUSER LOGIN");
        boolean loggingIsCorrect;
        String login;
        String password;
        do {
            String serverResponse = returnTheAnswerAsString(bufferedReader);
            LOGGER.info("\n{}", serverResponse);
            login = getValidLogin(bufferedReader, bufferedWriter, scanner);
            showIncomingMessage(bufferedReader);
            password = getValidPassword(scanner);
            sendMessage(bufferedWriter, password);
            serverResponse = returnTheAnswerAsString(bufferedReader);
            loggingIsCorrect = Boolean.parseBoolean(serverResponse);
            showIncomingMessage(bufferedReader);

        } while (!loggingIsCorrect);
        String handlingRights = returnTheAnswerAsString(bufferedReader);
        if (handlingRights.equals(HandlingRights.ADMIN.toString())) {
            handleAdminLogin(bufferedWriter, bufferedReader, scanner, login);
        } else if (handlingRights.equals(HandlingRights.USER.toString())) {
            handleUserLogin(bufferedWriter, bufferedReader, scanner, login);
        }
    }

    private String getValidLogin(BufferedReader bufferedReader, BufferedWriter bufferedWriter, Scanner scanner) {
        String serverResponse;
        boolean loginExistsInTheDatabase;
        String userLogin;
        do {
            String certification = "login";
            int minimumLoginLength = 3;
            userLogin = getUserInputCertification(scanner, certification, minimumLoginLength) + System.lineSeparator();
            sendMessage(bufferedWriter, userLogin);
            serverResponse = returnTheAnswerAsString(bufferedReader);
            loginExistsInTheDatabase = Boolean.parseBoolean(serverResponse);
            if (!loginExistsInTheDatabase) {
                LOGGER.info("\nThe specified login not exists in the database.\n\nEnter a correct login:");
            }
        } while (!loginExistsInTheDatabase);
        return userLogin;
    }

    private String getValidPassword(Scanner scanner) {
        String certification = "password";
        int minimumLoginLength = 8;
        return getUserInputCertification(scanner, certification, minimumLoginLength) + System.lineSeparator();
    }

    private void handleUserLogin
            (BufferedWriter bufferedWriter, BufferedReader bufferedReader, Scanner scanner, String login) {
        UserMenu userMenu = new UserMenu();
        userMenu.userMenuHandle(bufferedWriter, bufferedReader, scanner, login);
    }

    private void handleAdminLogin
            (BufferedWriter bufferedWriter, BufferedReader bufferedReader, Scanner scanner, String login) {
        AdminMenu adminMenu = new AdminMenu();
        adminMenu.adminMenuHandle(bufferedWriter, bufferedReader, scanner, login);
    }

}
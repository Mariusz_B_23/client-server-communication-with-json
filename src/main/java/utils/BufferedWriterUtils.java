package utils;

import exceptions.BufferedWriterException;
import validation.ValidationService;

import java.io.BufferedWriter;
import java.io.IOException;

public class BufferedWriterUtils {

    private BufferedWriterUtils() {
    }

    public static void sendMessage(BufferedWriter bufferedWriter, String messageToSend) {
        ValidationService.isBufferedWriterValid(bufferedWriter);
        ValidationService.isStringValid(messageToSend);
        try {
            bufferedWriter.write(messageToSend);
            bufferedWriter.newLine();
            bufferedWriter.flush();
        } catch (IOException e) {
            throw new BufferedWriterException("Error occurred while writing to BufferedWriter.", e);
        }
    }

}
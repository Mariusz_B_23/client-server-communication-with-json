package utils;

import client.message.MessageHandle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Scanner;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedReaderUtils.showIncomingMessage;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class CommonMethods {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public String getUserInputNonEmpty(Scanner scanner) {
        isScannerValid(scanner);
        String answer;
        do {
            answer = scanner.nextLine().toLowerCase();
            if (answer.isEmpty()) {
                LOGGER.info("Empty content. Enter the correct value:");
            }
        } while (answer.isEmpty());
        return answer;
    }

    public String getUserConfirmationForDeletion(Scanner scanner, String certification) {
        isScannerValid(scanner);
        isStringValid(certification);
        String answer;
        do {
            answer = scanner.nextLine().toLowerCase();
            if (!answer.equals("y") && !answer.equals("n")) {
                LOGGER.info("Enter y (yes) to delete your {} or n (no) to opt out of having your {} deleted.",
                        certification, certification);
            }
        } while (!answer.equals("y") && !answer.equals("n"));
        return answer;
    }

    public void handleOptionCommandsUntilBack
            (BufferedReader bufferedReader, Scanner scanner, BufferedWriter bufferedWriter) {
        isBufferedReaderValid(bufferedReader);
        isBufferedWriterValid(bufferedWriter);
        isScannerValid(scanner);
        boolean isSelectedBack;
        do {
            showIncomingMessage(bufferedReader);
            String newQuery = getUserInputNonEmpty(scanner) + System.lineSeparator();
            isSelectedBack = newQuery.trim().equals("back");
            sendMessage(bufferedWriter, newQuery);
            showIncomingMessage(bufferedReader);
        } while (!isSelectedBack);
    }

    public void handleMessagesUntilBack(BufferedReader bufferedReader, Scanner scanner, BufferedWriter bufferedWriter) {
        isBufferedReaderValid(bufferedReader);
        isBufferedWriterValid(bufferedWriter);
        isScannerValid(scanner);
        boolean isSelectedBack;
        do {
            showIncomingMessage(bufferedReader);
            String messageCommand = getUserInputNonEmpty(scanner) + System.lineSeparator();
            sendMessage(bufferedWriter, messageCommand);
            String isSelectedBackAsString = returnTheAnswerAsString(bufferedReader);
            isSelectedBack = Boolean.parseBoolean(isSelectedBackAsString);
            if (isSelectedBack) {
                showIncomingMessage(bufferedReader);
            } else {
                MessageHandle messageManager = new MessageHandle();
                messageManager.processMessageCommands(bufferedReader, messageCommand, scanner, bufferedWriter);
            }
        } while (!isSelectedBack);
    }

    public void handleUserInputAndValidation
            (BufferedWriter bufferedWriter, Scanner scanner, BufferedReader bufferedReader) {
        isBufferedReaderValid(bufferedReader);
        isScannerValid(scanner);
        isBufferedWriterValid(bufferedWriter);
        showIncomingMessage(bufferedReader);
        boolean loginIsCorrect;
        CommonMethods commonMethods = new CommonMethods();
        do {
            String login = commonMethods.getUserInputNonEmpty(scanner) + System.lineSeparator();
            sendMessage(bufferedWriter, login);
            String loginIsCorrectAsString = returnTheAnswerAsString(bufferedReader);
            loginIsCorrect = Boolean.parseBoolean(loginIsCorrectAsString);
        } while (!loginIsCorrect);
        showIncomingMessage(bufferedReader);
        showIncomingMessage(bufferedReader);
    }

}
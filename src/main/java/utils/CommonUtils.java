package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Scanner;

import static users.admin.AdminManagement.*;
import static users.user.UserManagement.doesLoginExistAsUser;
import static users.user.UserManagement.readAllUsersFromJsonFile;
import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class CommonUtils {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public static void loadAdminsAndUsersFromJsonFilesToLists() {
        readAllAdminsFromJsonFile();
        readAllUsersFromJsonFile();
    }

    public static void sendInitialMessagesToClient(BufferedWriter bufferedWriter) {
        isBufferedWriterValid(bufferedWriter);
        String numberOfMessagesSent = "2" + System.lineSeparator();
        sendMessage(bufferedWriter, numberOfMessagesSent);
        sendMessage(bufferedWriter, getListOfOptionsForUserManagement());
        sendMessage(bufferedWriter, getEnterCommandPrompt());
    }

    public void receiveInitialInstructionsFromServer(BufferedReader bufferedReader) {
        int numberOfMessagesReceivedAsInt = readNumberOfMessages(bufferedReader);
        for (int i = 0; i < numberOfMessagesReceivedAsInt; i++) {
            String responseFromServer = returnTheAnswerAsString(bufferedReader);
            LOGGER.info("\n{}", responseFromServer);

        }
    }

    private int readNumberOfMessages(BufferedReader bufferedReader) {
        return Integer.parseInt(returnTheAnswerAsString(bufferedReader));
    }

    private static String getListOfOptionsForUserManagement() {
        return "Choose one of the available options:" + System.lineSeparator()
                + "1     -> sign up" + System.lineSeparator()
                + "2     -> log in" + System.lineSeparator()
                + "stop  -> disconnection with the server" + System.lineSeparator();

    }

    private static String getEnterCommandPrompt() {
        return "Enter command:" + System.lineSeparator();
    }


    public static boolean checkingIfALoginExistsInTheJsonFile(String login) {
        isStringValid(login);
        return doesLoginExistAsUser(login)
                || doesLoginExistAsAdmin(login);
    }

    public static String getUserInputCertification(Scanner scanner, String certification, int minimumLength) {
        isScannerValid(scanner);
        String userInput;
        do {
            userInput = scanner.nextLine();
            if (userInput.length() < minimumLength) {
                LOGGER.info("\nThe {} must have a minimum of {} characters.\n\nPlease enter again:", certification, minimumLength);
            }
        } while (userInput.length() < minimumLength);
        return userInput;
    }

    public static void handleEmptyAdminList(BufferedWriter bufferedWriter) {
        isBufferedWriterValid(bufferedWriter);
        String noAdminAccountMessage =
                "No accounts. Select the registration option and create an administrator account."
                        + System.lineSeparator();
        LOGGER.info("{}",noAdminAccountMessage);
        sendMessage(bufferedWriter, noAdminAccountMessage);
    }

    public static boolean hasAtLeastOneAdministratorAccount() {
        return !getAdminList().isEmpty();
    }

}
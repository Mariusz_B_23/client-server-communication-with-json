package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

import static validation.ValidationService.isPropertiesValid;
import static validation.ValidationService.isStringValid;

public class PropertiesUtils {

    private PropertiesUtils() {
    }

    private static final String CONFIG_PROPERTIES_FILENAME = "config.properties";
    private static final Properties CONFIG_PROPERTIES = loadConfigPropertiesFromFile();
    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    private static Properties loadConfigPropertiesFromFile() {
        Properties properties = new Properties();
        try (InputStream inputStream =
                     PropertiesUtils.class.getClassLoader().getResourceAsStream(CONFIG_PROPERTIES_FILENAME)) {
            if (inputStream != null) {
                properties.load(inputStream);
            } else {
                throw new IOException("Unable to load configuration file: " + CONFIG_PROPERTIES_FILENAME);
            }
        } catch (IOException e) {
            Objects.requireNonNull(LOGGER).error("An error occurred while reading the configuration file: {}", e.getMessage());
        }
        return properties;
    }

    private static String getConfigPropertyFromProperties(String propertyName) {
        isPropertiesValid(CONFIG_PROPERTIES);
        isStringValid(propertyName);
        return CONFIG_PROPERTIES.getProperty(propertyName);
    }

    public static String getAdminsJsonFilepath() {
        return getConfigPropertyFromProperties("ADMINS_JSON_FILEPATH");
    }

    public static String getUsersJsonFilepath() {
        return getConfigPropertyFromProperties("USERS_JSON_FILEPATH");
    }

    public static int getServerPortNumber() {
        return Integer.parseInt(getConfigPropertyFromProperties("SERVER_PORT_NUMBER"));
    }

    public static String getClientIP() {
        return getConfigPropertyFromProperties("CLIENT_IP");
    }

    public static String getServerVersion() {
        return getConfigPropertyFromProperties("SERVER_VERSION");
    }

    public static String getCreationServerDate() {
        return getConfigPropertyFromProperties("CREATION_SERVER_DATE");
    }

}
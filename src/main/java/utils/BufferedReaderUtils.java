package utils;

import exceptions.ResponseFromTheServerException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;

import static validation.ValidationService.isBufferedReaderValid;

public class BufferedReaderUtils {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    private BufferedReaderUtils() {
    }

    private static String readResponse(BufferedReader bufferedReader) {
        isBufferedReaderValid(bufferedReader);
        StringBuilder respond = new StringBuilder();
        String line;
        try {
            while ((line = bufferedReader.readLine()) != null
                    && !line.isEmpty()) {
                respond.append(line).append(System.lineSeparator());
            }
            return respond.toString().trim();
        } catch (IOException e) {
            throw new
                    ResponseFromTheServerException("An error occurred while reading the response from the server.", e);
        }
    }

    public static void showIncomingMessage(BufferedReader bufferedReader) {
        isBufferedReaderValid(bufferedReader);
        LOGGER.info("\n{}", readResponse(bufferedReader));
    }

    public static String returnTheAnswerAsString(BufferedReader bufferedReader) {
        isBufferedReaderValid(bufferedReader);
        return readResponse(bufferedReader);
    }

}
package message;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public final class Message {
    private String recipient;
    private LocalDateTime receivedDate;
    private boolean isRead;
    private String sender;
    private String content;
    private static final int MAXIMUM_MESSAGE_LENGTH = 255;


    public Message(String recipient, LocalDateTime receivedDate, String sender, String content) {
        this.recipient = recipient;
        this.receivedDate = receivedDate;
        this.isRead = false;
        this.sender = sender;
        this.content = content;
    }

    public static int getMaximumLength() {
        return MAXIMUM_MESSAGE_LENGTH;
    }

    public boolean getIsRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    @Override
    public String toString() {
        return "Recipient        ->   " + recipient
                + System.lineSeparator()
                + "Received date    ->   " + receivedDate
                + System.lineSeparator()
                + "Sender           ->   " + sender
                + System.lineSeparator()
                + "Content          ->   " + content;
    }

}
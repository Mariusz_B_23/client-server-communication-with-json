package server.menu;

import commands.CommandsHandle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.account.deletion.AccountDeletionManager;
import server.account.managing.ChangePasswordManager;
import server.messages.MessageManager;
import users.user.User;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.time.Instant;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class UserMenuManager {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void userMenuHandle
            (User user, BufferedWriter bufferedWriter, BufferedReader bufferedReader, Instant serverLaunchTime) {
        isBaseUserValid(user);
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isServerLaunchTimeValid(serverLaunchTime);
        boolean logout = false;
        CommandsHandle commandsHandle = new CommandsHandle();
        do {
            sendMessage(bufferedWriter, getUserOptionsList());
            String userChooseOption = returnTheAnswerAsString(bufferedReader);
            if (userChooseOption.equals("5")) {
                logout = true;
                commandsHandle.logoutHandling(user, bufferedWriter);
            } else if (userChooseOption.equals("4")) {
                AccountDeletionManager accountDeletionManager = new AccountDeletionManager();
                logout = accountDeletionManager
                        .deletionConfirmed(bufferedWriter, bufferedReader, user, userChooseOption);
            } else {
                switch (userChooseOption) {
                    case "1" -> {
                        LOGGER.info("Chosen command - {} - Commands", userChooseOption);
                        commandsHandle.handleHelperCommandsMenu(bufferedWriter, bufferedReader, serverLaunchTime);
                    }
                    case "2" -> {
                        LOGGER.info("Chosen command - {} - Messages", userChooseOption);
                        MessageManager messageManager = new MessageManager();
                        messageManager.processMessageMenuOptions(user, bufferedWriter, bufferedReader);
                    }
                    case "3" -> {
                        LOGGER.info("Chosen command - {} - Change password", userChooseOption);
                        ChangePasswordManager changePasswordManager = new ChangePasswordManager();
                        changePasswordManager.changePasswordPerform(bufferedWriter, bufferedReader, user);
                    }
                    default -> {
                        LOGGER.info("Chosen bad command - \"{}\" - is wrong.", userChooseOption);
                        commandsHandle.processWrongCommand(bufferedWriter);
                    }
                }
            }
        } while (!logout);
    }

    private static String getUserOptionsList() {
        return "USER MENU - enter number to select option" + System.lineSeparator()
                + "1  ->  Commands" + System.lineSeparator()
                + "2  ->  Messages" + System.lineSeparator()
                + "3  ->  Change password" + System.lineSeparator()
                + "4  ->  Delete account" + System.lineSeparator()
                + "5  ->  Log out" + System.lineSeparator()
                + "Enter command:" + System.lineSeparator();
    }

}
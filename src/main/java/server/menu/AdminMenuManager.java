package server.menu;

import commands.CommandsHandle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.account.deletion.AccountDeletionManager;
import server.account.managing.ChangePasswordManager;
import server.account.managing.ManagingOtherAccount;
import server.messages.MessageManager;
import users.admin.Admin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.time.Instant;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class AdminMenuManager {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void adminMenuHandle
            (Admin admin, BufferedWriter bufferedWriter, BufferedReader bufferedReader, Instant serverLaunchTime) {
        isBaseUserValid(admin);
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isServerLaunchTimeValid(serverLaunchTime);
        boolean logout = false;
        CommandsHandle commandsHandle = new CommandsHandle();
        do {
            sendMessage(bufferedWriter, getAdminOptionsList());
            String adminChooseOption = returnTheAnswerAsString(bufferedReader);
            if (adminChooseOption.equals("6")) {
                logout = true;
                commandsHandle.logoutHandling(admin, bufferedWriter);
            } else if (adminChooseOption.equals("4")) {
                AccountDeletionManager accountDeletionManager = new AccountDeletionManager();
                logout = accountDeletionManager
                        .deletionConfirmed(bufferedWriter, bufferedReader, admin, adminChooseOption);
            } else {
                switch (adminChooseOption) {
                    case "1" -> {
                        LOGGER.info("Chosen command - {} - Commands", adminChooseOption);
                        commandsHandle.handleHelperCommandsMenu(bufferedWriter, bufferedReader, serverLaunchTime);
                    }
                    case "2" -> {
                        LOGGER.info("Chosen command - {} - Messages", adminChooseOption);
                        MessageManager messageManager = new MessageManager();
                        messageManager.processMessageMenuOptions(admin, bufferedWriter, bufferedReader);
                    }
                    case "3" -> {
                        LOGGER.info("Chosen command - {} - Change password", adminChooseOption);
                        ChangePasswordManager changePasswordManager = new ChangePasswordManager();
                        changePasswordManager.changePasswordPerform(bufferedWriter, bufferedReader, admin);
                    }
                    case "5" -> {
                        LOGGER.info("Chosen command - {} - Managing other users' accounts", adminChooseOption);
                        ManagingOtherAccount managingOtherAccount = new ManagingOtherAccount();
                        managingOtherAccount.chooseOption(bufferedWriter, bufferedReader, admin);
                    }
                    default -> {
                        LOGGER.info("Chosen bad command - \"{}\" - is wrong.", adminChooseOption);
                        commandsHandle.processWrongCommand(bufferedWriter);
                    }
                }
            }
        } while (!logout);
    }

    private static String getAdminOptionsList() {
        return "ADMIN MENU - enter number to select option" + System.lineSeparator()
                + "1  ->  Commands" + System.lineSeparator()
                + "2  ->  Messages" + System.lineSeparator()
                + "3  ->  Change password" + System.lineSeparator()
                + "4  ->  Delete account" + System.lineSeparator()
                + "5  ->  Managing other users' accounts" + System.lineSeparator()
                + "6  ->  Log out" + System.lineSeparator()
                + "Enter command:" + System.lineSeparator();
    }

}
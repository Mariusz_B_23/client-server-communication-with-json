package server.messages;

import commands.CommandsHandle;
import database.DSLContextProvider;
import database.MessageRepository;
import database.UserRepository;
import json.JsonWriter;
import mailbox.UnreadBox;
import message.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import users.BaseUser;
import users.admin.Admin;
import users.admin.AdminManagement;
import users.user.User;
import users.user.UserManagement;
import utils.HandlingRights;
import validation.ValidationService;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static users.admin.AdminManagement.doesLoginExistAsAdmin;
import static users.user.UserManagement.doesLoginExistAsUser;
import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class MessageManager {

    private final JsonWriter jsonWriter = new JsonWriter();
    private static final DSLContext DSL_CONTEXT = DSLContextProvider.createDslContext();
    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void processMessageMenuOptions
            (BaseUser baseUser, BufferedWriter bufferedWriter, BufferedReader bufferedReader) {
        boolean isSelectedBack;
        ValidationService.isBaseUserValid(baseUser);
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        do {
            String message = "MESSAGES - select option" + System.lineSeparator()
                    + "1 - write" + System.lineSeparator()
                    + "2 - unread" + System.lineSeparator()
                    + "3 - read" + System.lineSeparator()
                    + "4 - delete" + System.lineSeparator()
                    + "back - exit to menu" + System.lineSeparator()
                    + "Enter command:" + System.lineSeparator();
            sendMessage(bufferedWriter, message);
            String commandMessage = returnTheAnswerAsString(bufferedReader);
            isSelectedBack = commandMessage.equals("back");
            String isSelectedBackAsString = isSelectedBack + System.lineSeparator();
            sendMessage(bufferedWriter, isSelectedBackAsString);
            if (isSelectedBack) {
                LOGGER.info("Chosen command - {}", commandMessage);
                CommandsHandle commandsHandle = new CommandsHandle();
                commandsHandle.processBackCommand(bufferedWriter);
            } else {
                processMessageMenu(baseUser, bufferedWriter, commandMessage, bufferedReader);
            }
        } while (!isSelectedBack);
    }

    private void processMessageMenu(BaseUser baseUser, BufferedWriter bufferedWriter, String commandMessage, BufferedReader bufferedReader) {
        isBaseUserValid(baseUser);
        isBufferedWriterValid(bufferedWriter);
        isStringValid(commandMessage);
        MessageRepository messageRepository = new MessageRepository(DSL_CONTEXT);
        switch (commandMessage) {
            case "1" -> {
                LOGGER.info("Chosen command - {} - write message", commandMessage);
                String selectMessageRecipientHeadline =
                        "Enter the nickname of the recipient to whom you want to send the message:"
                                + System.lineSeparator();
                sendMessage(bufferedWriter, selectMessageRecipientHeadline);
                boolean isTheUserInTheList;
                String nicknameProvidedByUser;
                do {
                    nicknameProvidedByUser = returnTheAnswerAsString(bufferedReader);
                    UserRepository userRepository = new UserRepository(DSL_CONTEXT);
                    isTheUserInTheList = userRepository.checkingIfAUserLoginExists(nicknameProvidedByUser);
                    //sprawdzenie w json
//                    isTheUserInTheList = checkingTheNicknameInTheList(nicknameProvidedByUser);
                    String isTheUserInTheDatabaseAsString = isTheUserInTheList + System.lineSeparator();
                    sendMessage(bufferedWriter, isTheUserInTheDatabaseAsString);
                    if (!isTheUserInTheList) {
                        String msg =
                                String.format("User \"%s\" does not exist in the database.%n", nicknameProvidedByUser);
                        LOGGER.info("{}", msg);
                        sendMessage(bufferedWriter, msg);
                        String msg2 = "Enter the correct nickname:" + System.lineSeparator();
                        sendMessage(bufferedWriter, msg2);
                    }
                } while (!isTheUserInTheList);
                LOGGER.info("User have entered the correct addressee's nickname - {}", nicknameProvidedByUser);
                boolean isUserUnreadMessagesBoxFull = isUserUnreadMessagesBoxFull(nicknameProvidedByUser);
                String isUserUnreadMessagesBoxFullAsString = isUserUnreadMessagesBoxFull + System.lineSeparator();
                sendMessage(bufferedWriter, isUserUnreadMessagesBoxFullAsString);
                if (isUserUnreadMessagesBoxFull) {
                    LOGGER.info("The \" {} \" has no space in the inbox.", nicknameProvidedByUser);
                    LOGGER.info("The {} has no space in the inbox.", nicknameProvidedByUser);
                    String unreadBoxIsFull =
                            String.format("The \"%s\" has no space in the inbox. " +
                                            "You are unable to send him/her a message.\n"
                                    , nicknameProvidedByUser);
                    sendMessage(bufferedWriter, unreadBoxIsFull);
                } else {
                    String typeMessageHeadline = "Write a message (maximum 255 characters):" + System.lineSeparator();
                    sendMessage(bufferedWriter, typeMessageHeadline);
                    boolean isTheMessageTooLong;
                    String messageByUser;
                    do {
                        messageByUser = returnTheAnswerAsString(bufferedReader);
                        isTheMessageTooLong = checkingMessageLength(messageByUser);
                        String isTheMessageTooLongAsString = isTheMessageTooLong + System.lineSeparator();
                        sendMessage(bufferedWriter, isTheMessageTooLongAsString);
                        if (isTheMessageTooLong) {
                            String msg =
                                    String.format("The message is too long as it has %d characters. " +
                                                    "The maximum length is %d characters."
                                            , messageByUser.length(), Message.getMaximumLength())
                                            + System.lineSeparator();
                            LOGGER.info("{}", msg);
                            sendMessage(bufferedWriter, msg);
                            String msg2 = "Write a message as required::" + System.lineSeparator();
                            sendMessage(bufferedWriter, msg2);
                        }
                    } while (isTheMessageTooLong);
                    LOGGER.info("The user entered the message correctly.");
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
                    String dateTimeString = LocalDateTime.now().format(formatter);
                    LocalDateTime dateTime = LocalDateTime.parse(dateTimeString, formatter);
                    Message messageToSend = new Message(nicknameProvidedByUser, dateTime, baseUser.getLogin(), messageByUser);
                    UnreadBox unreadBox = new UnreadBox();
                    unreadBox.addUnreadMessageToRecipientUnreadBox(nicknameProvidedByUser, messageToSend);
                    String confirmationOfMessageSending =
                            String.format("The message to user \"" + nicknameProvidedByUser + "\" has been sent.")
                                    + System.lineSeparator();
                    sendMessage(bufferedWriter, confirmationOfMessageSending);
                    if (doesLoginExistAsUser(nicknameProvidedByUser)) {
                        jsonWriter.addMessageToUserUnreadBoxInJsonFile(nicknameProvidedByUser, messageToSend);
                    } else if (doesLoginExistAsAdmin(nicknameProvidedByUser)) {
                        jsonWriter.addMessageToAdminUnreadBoxInJsonFile(nicknameProvidedByUser, messageToSend);
                    }
                    messageRepository.createMessage(messageToSend);
                }
            }
            case "2" -> {
                LOGGER.info("Chosen command - {} - unread messages", commandMessage);
                int numberOfUnreadMessages = baseUser.getMailBox().getUnreadBox().getUnreadMessages().size();
                LOGGER.info("Number of unread messages: {}", numberOfUnreadMessages);
                String numberOfUnreadMessagesAsString = numberOfUnreadMessages + System.lineSeparator();
                sendMessage(bufferedWriter, numberOfUnreadMessagesAsString);
                String unreadHeadline = "Unread message [" + numberOfUnreadMessages + "]" + System.lineSeparator();
                sendMessage(bufferedWriter, unreadHeadline);
                if (numberOfUnreadMessages > 0) {
                    for (int i = 0; i < numberOfUnreadMessages; i++) {
                        String message = "------------------------------" + System.lineSeparator() +
                                "Message nr " + (i + 1) + System.lineSeparator() +
                                baseUser.getMailBox().getUnreadBox().getMessage(i) + System.lineSeparator()
                                + "------------------------------" + System.lineSeparator();
                        sendMessage(bufferedWriter, message);
                        baseUser.getMailBox().getUnreadBox().getUnreadMessages().get(i).setRead(true);
                    }
                }
                markAllUnreadMessagesAsRead(baseUser);
                messageRepository.changeTheReadingStatus(baseUser);
                if (baseUser.getHandlingRights().equals(HandlingRights.USER)) {
                    jsonWriter.saveUserListToJsonFile();
                } else if (baseUser.getHandlingRights().equals(HandlingRights.ADMIN)) {
                    jsonWriter.saveAdminListToJsonFile();
                }
            }
            case "3" -> {
                LOGGER.info("Chosen command - {} - read messages", commandMessage);
                int numberOfMessagesInTheInbox = baseUser.getMailBox().getInbox().getReadMessages().size();
                LOGGER.info("Number of messages in the inbox: {}", numberOfMessagesInTheInbox);
                String numberOfMessagesInTheInboxAsString = numberOfMessagesInTheInbox + System.lineSeparator();
                sendMessage(bufferedWriter, numberOfMessagesInTheInboxAsString);
                String readHeadline = "Read message [" + numberOfMessagesInTheInbox + "]" + System.lineSeparator();
                sendMessage(bufferedWriter, readHeadline);
                if (numberOfMessagesInTheInbox > 0) {
                    for (int i = 0; i < baseUser.getMailBox().getInbox().getReadMessages().size(); i++) {
                        String message = "------------------------------" + System.lineSeparator() +
                                "Message nr " + (i + 1) + System.lineSeparator() +
                                baseUser.getMailBox().getInbox().getSelectedMessage(i) + System.lineSeparator()
                                + "------------------------------" + System.lineSeparator();
                        sendMessage(bufferedWriter, message);
                    }
                }
            }
            case "4" -> {
                LOGGER.info("Chosen command - {} - delete message(s)", commandMessage);
                boolean inboxIsEmpty = baseUser.getMailBox().getInbox().getReadMessages().isEmpty();
                String inboxIsEmptyAsString = inboxIsEmpty + System.lineSeparator();
                sendMessage(bufferedWriter, inboxIsEmptyAsString);

                if (inboxIsEmpty) {
                    String emptyInboxMessage = "There are no messages to delete because the inbox is empty."
                            + System.lineSeparator();
                    sendMessage(bufferedWriter, emptyInboxMessage);
                    LOGGER.info("{}", emptyInboxMessage);
                } else {
                    String selectMessageRecipientHeadline =
                            "Select the option :" + System.lineSeparator()
                                    + "s - delete the selected message" + System.lineSeparator()
                                    + "a - delete all messages" + System.lineSeparator()
                                    + "Enter option:" + System.lineSeparator();
                    sendMessage(bufferedWriter, selectMessageRecipientHeadline);
                    String userChooseOptionDeletion = returnTheAnswerAsString(bufferedReader);
                    if (userChooseOptionDeletion.equals("s")) {
                        LOGGER.info("User {} chose to delete a single message.", baseUser.getLogin());
                        boolean correctMessageIndex;
                        do {
                            String enterMessageNumber = "Enter the number of the message you wish to delete."
                                    + System.lineSeparator();
                            sendMessage(bufferedWriter, enterMessageNumber);
                            String indexOfMessageAsString = returnTheAnswerAsString(bufferedReader);
                            int indexOfMessage = (Integer.parseInt(indexOfMessageAsString));
                            Message messageToDelete;
                            correctMessageIndex = checkTheCorrectnessOfTheGivenIndex(indexOfMessage, baseUser);
                            String correctMessageIndexAsString = correctMessageIndex + System.lineSeparator();
                            sendMessage(bufferedWriter, correctMessageIndexAsString);
                            if (correctMessageIndex) {
                                messageToDelete = baseUser.getMailBox().getInbox().getMessage(indexOfMessage - 1);
                                messageRepository.deleteSingleMessage(messageToDelete);
                                baseUser.getMailBox().getInbox().removeSelectedMessageFromInbox(indexOfMessage);
                                if (baseUser.getHandlingRights().equals(HandlingRights.USER)) {
                                    jsonWriter.removeMessageFromUserInBoxInJsonFile(baseUser, indexOfMessage);
                                } else if (baseUser.getHandlingRights().equals(HandlingRights.ADMIN)) {
                                    jsonWriter.removeMessageFromAdminInBoxInJsonFile(baseUser, indexOfMessage);
                                }
                                String deleteMessage = "The message number " + indexOfMessage + " was correctly deleted."
                                        + System.lineSeparator();
                                sendMessage(bufferedWriter, deleteMessage);
                                LOGGER.info("{}", deleteMessage);
                            } else {
                                String invalidNumber = "An invalid message number for deletion was specified - "
                                        + indexOfMessage + "." + System.lineSeparator();
                                sendMessage(bufferedWriter, invalidNumber);
                                LOGGER.info("{}", indexOfMessage);
                            }
                        } while (!correctMessageIndex);
                    } else if (userChooseOptionDeletion.equals("a")) {
                        LOGGER.info("User {} chose to delete all messages.", baseUser.getLogin());
                        String confirmDeletion = "Are you sure you want to delete all messages from your inbox? " +
                                "y - yes / n - no" + System.lineSeparator();
                        sendMessage(bufferedWriter, confirmDeletion);
                        String userChooseYesOrNo = returnTheAnswerAsString(bufferedReader).toLowerCase();
                        if (userChooseYesOrNo.equals("y")) {
                            baseUser.getMailBox().getInbox().clearAllMessagesFromInbox();
                            baseUser.getMailBox().getUnreadBox().clearAllMessagesFromUnreadbox();
                            messageRepository.deleteAllMessages(baseUser);
                            if (baseUser.getHandlingRights().equals(HandlingRights.USER)) {
                                jsonWriter.saveUserListToJsonFile();
                            } else if (baseUser.getHandlingRights().equals(HandlingRights.ADMIN)) {
                                jsonWriter.saveAdminListToJsonFile();
                            }
                            String deleteConfirm = "All messages from the inbox have been deleted." + System.lineSeparator();
                            sendMessage(bufferedWriter, deleteConfirm);
                            LOGGER.info("{}", deleteConfirm);
                        } else if (userChooseYesOrNo.equals("n")) {
                            String deleteCancel = "The deletion of all messages was cancelled.." + System.lineSeparator();
                            sendMessage(bufferedWriter, deleteCancel);
                            LOGGER.info("{}", deleteCancel);
                        }
                    }
                }
            }
            default -> {
                CommandsHandle commandsHandle = new CommandsHandle();
                commandsHandle.processWrongCommand(bufferedWriter);
            }
        }
    }

    private boolean checkingTheNicknameInTheList(String nicknameProvidedByUser) {
        isStringValid(nicknameProvidedByUser);
        return doesLoginExistAsAdmin(nicknameProvidedByUser)
                || doesLoginExistAsUser(nicknameProvidedByUser);
    }

    private static boolean isUserUnreadMessagesBoxFull(String nicknameProvidedByUser) {
        for (User user : UserManagement.getUserList()) {
            if (user.getLogin().equals(nicknameProvidedByUser)) {
                return user.getMailBox().getUnreadBox().getUnreadMessages().size() == UnreadBox.getMaximumSizeOfList();
            }
        }
        for (Admin admin : AdminManagement.getAdminList()) {
            if (admin.getLogin().equals(nicknameProvidedByUser)) {
                return admin.getMailBox().getUnreadBox().getUnreadMessages().size() == UnreadBox.getMaximumSizeOfList();
            }
        }
        return false;
    }

    private boolean checkingMessageLength(String messageByUser) {
        isStringValid(messageByUser);
        return (messageByUser.length() > Message.getMaximumLength());
    }

    private boolean checkTheCorrectnessOfTheGivenIndex(int indexOfMessage, BaseUser baseUser) {
        int indexUpdatedToListNumbering = (indexOfMessage - 1);
        return indexUpdatedToListNumbering >= 0
                && indexUpdatedToListNumbering < baseUser.getMailBox().getInbox().getReadMessages().size();
    }

    public void markAllUnreadMessagesAsRead(BaseUser baseUser) {
        baseUser.getMailBox().getInbox().getReadMessages()
                .addAll(baseUser.getMailBox().getUnreadBox().getUnreadMessages());
        baseUser.getMailBox().getUnreadBox().getUnreadMessages().clear();
    }

}
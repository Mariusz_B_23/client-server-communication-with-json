package server.account.logging;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.menu.AdminMenuManager;
import server.menu.UserMenuManager;
import users.admin.Admin;
import users.admin.AdminManagement;
import users.user.User;
import users.user.UserManagement;
import utils.HandlingRights;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.time.Instant;

import static users.admin.AdminManagement.adminLoggingIsCorrect;
import static users.admin.AdminManagement.doesLoginExistAsAdmin;
import static users.user.UserManagement.userLoggingIsCorrect;
import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static utils.CommonUtils.*;
import static validation.ValidationService.*;

public class UserLoginManager {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void performLoginProcess(BufferedWriter bufferedWriter, BufferedReader bufferedReader,
                                    Instant serverLaunchTime) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isServerLaunchTimeValid(serverLaunchTime);
        LOGGER.info("\nChosen command - log in");
        if (!hasAtLeastOneAdministratorAccount()) {
            handleEmptyAdminList(bufferedWriter);
        } else {
            authenticateUser(bufferedWriter, bufferedReader, serverLaunchTime);
        }
    }

    private void authenticateUser
            (BufferedWriter bufferedWriter, BufferedReader bufferedReader, Instant serverLaunchTime) {
        String login;
        String password;
        boolean loggingIsCorrect;
        HandlingRights handlingRights;
        loadAdminsAndUsersFromJsonFilesToLists();
        do {
            login = getCorrectLogin(bufferedWriter, bufferedReader);
            password = getPassword(bufferedWriter, bufferedReader);
            loggingIsCorrect = loggingCheck(login, password);
            String loggingMessage;
            if (!loggingIsCorrect) {
                loggingMessage = "The user entered incorrect password when logging in.";
            } else {
                loggingMessage = "Logging in user \"" + login + "\" was successful.";
            }
            LOGGER.info("\n{}", loggingMessage);
            sendMessage(bufferedWriter, loggingIsCorrect + System.lineSeparator());
            sendMessage(bufferedWriter, loggingMessage + System.lineSeparator());
        } while (!loggingIsCorrect);
        handlingRights = getHandlingRightsForSelectedLogin(login);
        sendMessage(bufferedWriter, handlingRights + System.lineSeparator());
        if (handlingRights.equals(HandlingRights.ADMIN)) {
            handleAdminLogin(bufferedWriter, bufferedReader, login, password, serverLaunchTime);
        } else if (handlingRights.equals(HandlingRights.USER)) {
            handleUserLogin(bufferedWriter, bufferedReader, login, password, serverLaunchTime);
        }
    }

    private String getCorrectLogin(BufferedWriter bufferedWriter, BufferedReader bufferedReader) {
        boolean doesTheLoginAlreadyExistInTheDatabase;
        String login;
        String loginLabel = "Login: " + System.lineSeparator();
        sendMessage(bufferedWriter, loginLabel);
        do {
            login = returnTheAnswerAsString(bufferedReader).trim();
            doesTheLoginAlreadyExistInTheDatabase = checkingIfALoginExistsInTheJsonFile(login);
            printLoginStatus(login, doesTheLoginAlreadyExistInTheDatabase);
            sendMessage(bufferedWriter, doesTheLoginAlreadyExistInTheDatabase + System.lineSeparator());
        } while (!doesTheLoginAlreadyExistInTheDatabase);
        return login;
    }

    private String getPassword(BufferedWriter bufferedWriter, BufferedReader bufferedReader) {
        String passwordLabel = "Password: " + System.lineSeparator();
        sendMessage(bufferedWriter, passwordLabel);
        return returnTheAnswerAsString(bufferedReader);
    }

    private boolean loggingCheck(String login, String password) {
        isStringValid(login);
        isStringValid(password);
        return adminLoggingIsCorrect(login, password)
                || userLoggingIsCorrect(login, password);
    }

    private void printLoginStatus(String login, boolean doesTheLoginAlreadyExistInTheDatabase) {
        isStringValid(login);
        if (doesTheLoginAlreadyExistInTheDatabase) {
            LOGGER.info("Login entered: {}", login);
        } else {
            LOGGER.info("User provided a login that does not already exist in the database - \"{}\"", login);

        }
    }

    private HandlingRights getHandlingRightsForSelectedLogin(String login) {
        isStringValid(login);
        if (doesLoginExistAsAdmin(login)) {
            return HandlingRights.ADMIN;
        } else {
            return HandlingRights.USER;
        }
    }

    private void handleUserLogin(BufferedWriter bufferedWriter, BufferedReader bufferedReader, String login,
                                 String password, Instant serverLaunchTime) {
        isStringValid(login);
        isStringValid(password);
        UserManagement userManagement = new UserManagement();
        User user = userManagement.getUserFromList(login, password);
        UserMenuManager userMenuManager = new UserMenuManager();
        userMenuManager.userMenuHandle(user, bufferedWriter, bufferedReader, serverLaunchTime);
    }

    private void handleAdminLogin(BufferedWriter bufferedWriter, BufferedReader bufferedReader, String login,
                                  String password, Instant serverLaunchTime) {
        isStringValid(login);
        isStringValid(password);
        AdminManagement adminManagement = new AdminManagement();
        Admin admin = adminManagement.getAdminFromListByPasswordAndLogin(login, password);
        AdminMenuManager adminMenuManager = new AdminMenuManager();
        adminMenuManager.adminMenuHandle(admin, bufferedWriter, bufferedReader, serverLaunchTime);
    }

}
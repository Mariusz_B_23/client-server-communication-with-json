package server.account.deletion;

import database.DSLContextProvider;
import database.MessageRepository;
import database.UserRepository;
import json.JsonWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import users.BaseUser;
import users.admin.Admin;
import users.admin.AdminManagement;
import users.user.User;
import users.user.UserManagement;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import static users.admin.AdminManagement.isAdminPasswordValid;
import static users.user.UserManagement.isUserPasswordValid;
import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class AccountDeletionManager {

    private static final DSLContext DSL_CONTEXT = DSLContextProvider.createDslContext();
    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public boolean deletionConfirmed(BufferedWriter bufferedWriter, BufferedReader bufferedReader, BaseUser baseUser,
                                     String chosenOption) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isBaseUserValid(baseUser);
        isStringValid(chosenOption);
        LOGGER.info("\nChosen command - {} - Delete account", chosenOption);
        String accountDeletionQuestion = "Do you wish to delete your account? Enter y (yes) or n (no) to confirm."
                + System.lineSeparator();
        sendMessage(bufferedWriter, accountDeletionQuestion);
        String accountDeletionConfirmation = returnTheAnswerAsString(bufferedReader).toLowerCase();
        boolean isAccountDeletionConfirmed = accountDeletionConfirmation.equals("y");
        if (isAccountDeletionConfirmed) {
            handleAccountDeletionConfirmation(bufferedWriter, bufferedReader, baseUser);
        }
        return isAccountDeletionConfirmed;
    }

    private void handleAccountDeletionConfirmation
            (BufferedWriter bufferedWriter, BufferedReader bufferedReader, BaseUser baseUser) {
        String enterPasswordToConfirm = "Enter your password to confirm the deletion of your account:"
                + System.lineSeparator();
        sendMessage(bufferedWriter, enterPasswordToConfirm);
        boolean isTheCheckedPasswordCorrect;
        String userGivesHisPasswordToCheck;
        do {
            userGivesHisPasswordToCheck = returnTheAnswerAsString(bufferedReader);
            isTheCheckedPasswordCorrect =
                    verifyPassword(baseUser, userGivesHisPasswordToCheck.trim());
            String checkedPasswordCorrectAsString = isTheCheckedPasswordCorrect + System.lineSeparator();
            sendMessage(bufferedWriter, checkedPasswordCorrectAsString);
            if (isTheCheckedPasswordCorrect) {
                performAccountDeletion(bufferedWriter, baseUser);
            } else {
                handleIncorrectPassword(bufferedWriter);
            }
        } while (!isTheCheckedPasswordCorrect);
    }

    private void performAccountDeletion(BufferedWriter bufferedWriter, BaseUser baseUser) {
        removeAccount(baseUser);
        JsonWriter jsonWriter = new JsonWriter();
        if (baseUser instanceof User) {
            jsonWriter.saveUserListToJsonFile();
        } else if (baseUser instanceof Admin) {
            jsonWriter.saveAdminListToJsonFile();
        }
        String accountDeletionMessage = "Password correct. The account has been deleted." + System.lineSeparator();
        LOGGER.info("\n{}", accountDeletionMessage);
        sendMessage(bufferedWriter, accountDeletionMessage);
    }

    private void handleIncorrectPassword(BufferedWriter bufferedWriter) {
        String accountDeletionMessage = "Password incorrect. Enter again:" + System.lineSeparator();
        sendMessage(bufferedWriter, accountDeletionMessage);
        String accountDeletionMessageServer = "Password incorrect." + System.lineSeparator();
        LOGGER.info("\n{}", accountDeletionMessageServer);
    }

    private boolean verifyPassword(BaseUser baseUser, String password) {
        isStringValid(password);
        if (baseUser instanceof User) {
            return isUserPasswordValid((User) baseUser, password);
        } else if (baseUser instanceof Admin) {
            return isAdminPasswordValid((Admin) baseUser, password);
        }
        return false;
    }

    private void removeAccount(BaseUser baseUser) {
        if (baseUser instanceof User) {
            UserManagement.removeUserFromList((User) baseUser);
        } else if (baseUser instanceof Admin) {
            AdminManagement.removeAdminFromList((Admin) baseUser);
        }
        MessageRepository messageRepository = new MessageRepository(DSL_CONTEXT);
        messageRepository.deleteAllMessages(baseUser);
        UserRepository userRepository = new UserRepository(DSL_CONTEXT);
        userRepository.deleteUserByLogin(baseUser.getLogin());
    }

}
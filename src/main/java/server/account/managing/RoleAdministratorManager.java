package server.account.managing;

import database.DSLContextProvider;
import database.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import users.admin.Admin;
import users.user.User;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import static json.JsonWriter.addAdminToJson;
import static json.JsonWriter.removeUserFromJson;
import static users.user.UserManagement.*;
import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;

public class RoleAdministratorManager {

    private static final DSLContext DSL_CONTEXT = DSLContextProvider.createDslContext();
    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void roleAdministratorProcess(BufferedWriter bufferedWriter, BufferedReader bufferedReader) {
        String chooseUserLabel = "Enter the login of the user to whom you want to grant administrator privileges."
                + System.lineSeparator();
        sendMessage(bufferedWriter, chooseUserLabel);
        boolean loginIsCorrect;
        User user;
        do {
            String loginToChangePrivilegesByUser = returnTheAnswerAsString(bufferedReader);
            loginIsCorrect = doesLoginExistAsUser(loginToChangePrivilegesByUser);
            String loginIsCorrectAsString = loginIsCorrect + System.lineSeparator();
            sendMessage(bufferedWriter, loginIsCorrectAsString);
            user = getUserFromListByLogin(loginToChangePrivilegesByUser);
        } while (!loginIsCorrect);
        changeRoleFromUser(user);
        String confirmInfo =
                String.format("\"%s\" administrator privileges have been correctly assigned.", user.getLogin());
        LOGGER.info("{}", confirmInfo);
        sendMessage(bufferedWriter, confirmInfo + System.lineSeparator());
    }

    private void changeRoleFromUser(User user) {
        Admin admin = new Admin(user);
        removeUserFromList(user);
        removeUserFromJson(user);
        addAdminToJson(admin);
        UserRepository userRepository = new UserRepository(DSL_CONTEXT);
        userRepository.changeRoleToAdmin(user);
    }

}
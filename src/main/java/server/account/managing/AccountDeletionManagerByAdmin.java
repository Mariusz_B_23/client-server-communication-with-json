package server.account.managing;

import database.DSLContextProvider;
import database.MessageRepository;
import database.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import users.BaseUser;
import users.admin.Admin;
import users.user.User;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Objects;

import static json.JsonWriter.removeAdminFromJson;
import static json.JsonWriter.removeUserFromJson;
import static users.admin.AdminManagement.removeAdminFromList;
import static users.user.UserManagement.removeUserFromList;
import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class AccountDeletionManagerByAdmin {

    private static final DSLContext DSL_CONTEXT = DSLContextProvider.createDslContext();
    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void adminDeleteOtherAccount(BufferedWriter bufferedWriter, BufferedReader bufferedReader) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        String chooseUserLabel = "Enter the login of the account you want to delete:" + System.lineSeparator();
        sendMessage(bufferedWriter, chooseUserLabel);
        boolean loginIsCorrect = false;
        BaseUser baseUser = null;
        do {
            String loginOfTheAccountToBeDeleted = returnTheAnswerAsString(bufferedReader);
            //sprawdzeniew w json
//            if (doesLoginExistAsAdmin(loginOfTheAccountToBeDeleted)) {
//                baseUser = getAdminFromListByLogin(loginOfTheAccountToBeDeleted);
//                loginIsCorrect = true;
//            }
//            if (doesLoginExistAsUser(loginOfTheAccountToBeDeleted)) {
//                baseUser = getUserFromListByLogin(loginOfTheAccountToBeDeleted);
//                loginIsCorrect = true;
//            }
            UserRepository userRepository = new UserRepository(DSL_CONTEXT);
            loginIsCorrect = userRepository.checkingIfAUserLoginExists(loginOfTheAccountToBeDeleted);
            String loginIsCorrectAsString = loginIsCorrect + System.lineSeparator();
            sendMessage(bufferedWriter, loginIsCorrectAsString);
        } while (!loginIsCorrect);
        deleteProcess(baseUser);
        String confirmInfo =
                String.format("User account \"%s\" has been properly deleted.",
                        Objects.requireNonNull(baseUser).getLogin());
        LOGGER.info("{}", confirmInfo);
        sendMessage(bufferedWriter, confirmInfo + System.lineSeparator());
    }

    private void deleteProcess(BaseUser baseUser) {
        isBaseUserValid(baseUser);
        if (baseUser instanceof User) {
            removeUserFromList((User) baseUser);
            removeUserFromJson((User) baseUser);
        } else if (baseUser instanceof Admin) {
            removeAdminFromList((Admin) baseUser);
            removeAdminFromJson((Admin) baseUser);
        }
        MessageRepository messageRepository = new MessageRepository(DSL_CONTEXT);
        messageRepository.deleteAllMessages(baseUser);
        UserRepository userRepository = new UserRepository(DSL_CONTEXT);
        userRepository.deleteUserByLogin(baseUser.getLogin());
    }

}
package server.account.managing;

import database.DSLContextProvider;
import database.UserRepository;
import json.JsonWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import users.BaseUser;
import users.admin.Admin;
import users.admin.AdminManagement;
import users.user.User;
import users.user.UserManagement;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class ChangePasswordManager {

    private static final DSLContext DSL_CONTEXT = DSLContextProvider.createDslContext();
    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void changePasswordPerform(BufferedWriter bufferedWriter, BufferedReader bufferedReader,
                                      BaseUser baseUser) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isBaseUserValid(baseUser);
        String oldPassword = "Enter your old password:" + System.lineSeparator();
        sendMessage(bufferedWriter, oldPassword);
        String oldPasswordToConfirm = returnTheAnswerAsString(bufferedReader);
        boolean validPassword = oldPasswordToConfirm.equals(baseUser.getPassword());
        String validPasswordAsString = validPassword + System.lineSeparator();
        sendMessage(bufferedWriter, validPasswordAsString);

        if (validPassword) {
            String correctPassword = "The password you entered is correct." + System.lineSeparator();
            sendMessage(bufferedWriter, correctPassword);
            LOGGER.info("\n{}", correctPassword);
            String newPassword;
            String newPasswordEnterAgain;
            boolean newPasswordVerification;
            do {
                String enterNewPassword = "Enter new password:" + System.lineSeparator();
                sendMessage(bufferedWriter, enterNewPassword);
                newPassword = returnTheAnswerAsString(bufferedReader);
                LOGGER.info("New password: {}", newPassword);
                String repeatNewPassword = "Enter the new password again:" + System.lineSeparator();
                sendMessage(bufferedWriter, repeatNewPassword);
                newPasswordEnterAgain = returnTheAnswerAsString(bufferedReader);
                LOGGER.info("Repetition of the new password: {}", newPasswordEnterAgain);
                newPasswordVerification = newPassword.equals(newPasswordEnterAgain);
                String passwordVerification = newPasswordVerification + System.lineSeparator();
                sendMessage(bufferedWriter, passwordVerification);
                if (newPasswordVerification) {
                    changePasswordProcess(bufferedWriter, baseUser, newPassword);
                    UserRepository userRepository = new UserRepository(DSL_CONTEXT);
                    userRepository.changePassword(baseUser, newPassword);
                } else {
                    passwordChangeError(bufferedWriter);
                }
            } while (!newPasswordVerification);
        } else {
            String incorrectPassword = "The password you entered is incorrect." + System.lineSeparator();
            sendMessage(bufferedWriter, incorrectPassword);
            LOGGER.info("{}", incorrectPassword);
        }
    }

    private static void passwordChangeError(BufferedWriter bufferedWriter) {
        String passwordAreNotTheSame =
                "The passwords given are not the same. The password has not been changed."
                        + System.lineSeparator();
        sendMessage(bufferedWriter, passwordAreNotTheSame);
        LOGGER.info("{}", passwordAreNotTheSame);
    }

    private static void changePasswordProcess(BufferedWriter bufferedWriter, BaseUser baseUser, String newPassword) {
        changePassword(baseUser, newPassword);
        JsonWriter jsonWriter = new JsonWriter();
        if (baseUser instanceof User) {
            jsonWriter.changePasswordLineInUsersJsonFile(baseUser, newPassword);
        } else if (baseUser instanceof Admin) {
            jsonWriter.changePasswordLineInAdminsJsonFile(baseUser, newPassword);
        }
        String passwordAreTheSame =
                "The passwords given are the same. The password has been changed."
                        + System.lineSeparator();
        sendMessage(bufferedWriter, passwordAreTheSame);
        LOGGER.info("{}", passwordAreTheSame);
    }

    public static void changePassword(BaseUser baseUser, String newPassword) {
        if (baseUser instanceof User) {
            for (User user : UserManagement.getUserList())
                if (user.getLogin().equals(baseUser.getLogin())
                        && user.getPassword().equals(baseUser.getPassword())) {
                    user.setPassword(newPassword);
                }
        } else if (baseUser instanceof Admin) {
            for (Admin admin : AdminManagement.getAdminList())
                if (admin.getLogin().equals(baseUser.getLogin())
                        && admin.getPassword().equals(baseUser.getPassword())) {
                    admin.setPassword(newPassword);
                }
        }
    }

}
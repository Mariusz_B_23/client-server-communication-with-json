package server.account.managing;

import commands.CommandsHandle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import users.admin.Admin;
import users.admin.AdminManagement;
import users.user.UserManagement;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.*;

public class ManagingOtherAccount {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void chooseOption(BufferedWriter bufferedWriter, BufferedReader bufferedReader,
                             Admin admin) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isBaseUserValid(admin);
        sendAMessageWithAvailableOptions(bufferedWriter);
        boolean adminChooseOptionBack = false;
        CommandsHandle commandsHandle = new CommandsHandle();
        do {
            String optionSelectedByAdmin = returnTheAnswerAsString(bufferedReader);
            switch (optionSelectedByAdmin) {
                case "1" -> {
                    boolean isUserListEmpty = UserManagement.getUserList().isEmpty();
                    String isThereMinimumOneUserAccountAsString = isUserListEmpty + System.lineSeparator();
                    sendMessage(bufferedWriter, isThereMinimumOneUserAccountAsString);
                    if (isUserListEmpty) {
                        String lackOfUsers =
                                "Lack of a minimum of one registered user to be granted administrator privileges."
                                        + System.lineSeparator();
                        LOGGER.info("{}", lackOfUsers);
                        sendMessage(bufferedWriter, lackOfUsers);
                    } else {
                        grantAdminRoleToUser(bufferedWriter, bufferedReader, optionSelectedByAdmin);
                    }
                }
                case "2" -> {
                    boolean isOnlyOneAdminAccount = AdminManagement.getAdminList().size() == 1;
                    String isOnlyOneAdminAccountAsString = isOnlyOneAdminAccount + System.lineSeparator();
                    sendMessage(bufferedWriter, isOnlyOneAdminAccountAsString);
                    if (isOnlyOneAdminAccount) {
                        String onlyOneAdminCommunicat =
                                "There is only one administrator. Permissions cannot be changed."
                                        + System.lineSeparator();
                        LOGGER.info("{}", onlyOneAdminCommunicat);
                        sendMessage(bufferedWriter, onlyOneAdminCommunicat);
                    } else {
                        revokeAdminPrivilegesFromUser(bufferedWriter, bufferedReader, optionSelectedByAdmin);
                    }
                }
                case "3" -> {
                    boolean isOnlyOneRegisteredAccount = isOnlyOneRegisteredAccount();
                    String isOnlyOneRegisteredAccountAsString = isOnlyOneRegisteredAccount + System.lineSeparator();
                    sendMessage(bufferedWriter, isOnlyOneRegisteredAccountAsString);
                    if (isOnlyOneRegisteredAccount) {
                        String deleteAccountIsImpossible =
                                "Only one registered user, you can't delete other accounts due to lack of them."
                                        + System.lineSeparator();
                        LOGGER.info("{}", deleteAccountIsImpossible);
                        sendMessage(bufferedWriter, deleteAccountIsImpossible);
                    } else {
                        deleteUserOrAdminAccount(bufferedWriter, bufferedReader, optionSelectedByAdmin);
                    }
                }
                case "back" -> {
                    adminChooseOptionBack = true;
                    commandsHandle.processBackCommand(bufferedWriter);
                }
                default -> commandsHandle.processWrongCommand(bufferedWriter);
            }
        } while (!adminChooseOptionBack);
    }

    private void sendAMessageWithAvailableOptions(BufferedWriter bufferedWriter) {
        String availableOptions =
                "1 - give the user the admin range" + System.lineSeparator()
                        + "2 - revoke the administrator privileges of the selected user" + System.lineSeparator()
                        + "3 - delete admin/user account" + System.lineSeparator()
                        + "back - back to main menu" + System.lineSeparator()
                        + "Enter command:" + System.lineSeparator();
        sendMessage(bufferedWriter, availableOptions);
    }

    private void grantAdminRoleToUser
            (BufferedWriter bufferedWriter, BufferedReader bufferedReader, String optionSelectedByAdmin) {
        LOGGER.info("Chosen command - {} - give the user the admin range", optionSelectedByAdmin);
        RoleAdministratorManager roleAdministratorManager = new RoleAdministratorManager();
        roleAdministratorManager.roleAdministratorProcess(bufferedWriter, bufferedReader);
        sendAMessageWithAvailableOptions(bufferedWriter);
    }

    private void revokeAdminPrivilegesFromUser
            (BufferedWriter bufferedWriter, BufferedReader bufferedReader, String optionSelectedByAdmin) {
        LOGGER.info("Chosen command - {} - revoke the administrator privileges of the selected user", optionSelectedByAdmin);
        RevocationOfAdministratorPrivilegesManager revocationOfAdministratorPrivilegesManager =
                new RevocationOfAdministratorPrivilegesManager();
        revocationOfAdministratorPrivilegesManager.processOfChangingEntitlements(bufferedWriter, bufferedReader);
        sendAMessageWithAvailableOptions(bufferedWriter);
    }

    private void deleteUserOrAdminAccount
            (BufferedWriter bufferedWriter, BufferedReader bufferedReader, String optionSelectedByAdmin) {
        LOGGER.info("Chosen command - {} - delete admin/user account", optionSelectedByAdmin);
        AccountDeletionManagerByAdmin adminDeleteAccountWhichHeChoose = new AccountDeletionManagerByAdmin();
        adminDeleteAccountWhichHeChoose.adminDeleteOtherAccount(bufferedWriter, bufferedReader);
        sendAMessageWithAvailableOptions(bufferedWriter);
    }

    private boolean isOnlyOneRegisteredAccount() {
        return AdminManagement.getAdminList().size() == 1 && UserManagement.getUserList().isEmpty();
    }

}
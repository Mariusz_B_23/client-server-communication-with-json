package server.account.managing;

import database.DSLContextProvider;
import database.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import users.admin.Admin;
import users.user.User;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import static json.JsonWriter.addUserToJson;
import static json.JsonWriter.removeAdminFromJson;
import static users.admin.AdminManagement.*;
import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static validation.ValidationService.isBufferedWriterValid;

public class RevocationOfAdministratorPrivilegesManager {

    private static final DSLContext DSL_CONTEXT = DSLContextProvider.createDslContext();
    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void processOfChangingEntitlements(BufferedWriter bufferedWriter, BufferedReader bufferedReader) {
        isBufferedWriterValid(bufferedWriter);
        String chooseUserLabel = "Enter the login of the administrator to whom you want to revoke privileges."
                + System.lineSeparator();
        sendMessage(bufferedWriter, chooseUserLabel);
        boolean loginIsCorrect;
        Admin admin;
        do {
            String loginToChangePrivilegesByAdmin = returnTheAnswerAsString(bufferedReader);
            loginIsCorrect = doesLoginExistAsAdmin(loginToChangePrivilegesByAdmin);
            String loginIsCorrectAsString = loginIsCorrect + System.lineSeparator();
            sendMessage(bufferedWriter, loginIsCorrectAsString);
            admin = getAdminFromListByLogin(loginToChangePrivilegesByAdmin);
        } while (!loginIsCorrect);
        changeRoleFromAdmin(admin);
        String confirmInfo =
                String.format("\"%s\" administrator privileges were properly revoked.", admin.getLogin());
        LOGGER.info("{}", confirmInfo);
        sendMessage(bufferedWriter, confirmInfo + System.lineSeparator());
    }

    private void changeRoleFromAdmin(Admin admin) {
        User user = new User(admin);
        removeAdminFromList(admin);
        removeAdminFromJson(admin);
        addUserToJson(user);
        UserRepository userRepository = new UserRepository(DSL_CONTEXT);
        userRepository.changeRoleToUser(admin);
    }

}
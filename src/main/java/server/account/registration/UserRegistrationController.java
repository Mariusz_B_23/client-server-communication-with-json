package server.account.registration;

import database.DSLContextProvider;
import database.UserRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import users.admin.Admin;
import users.admin.AdminManagement;
import users.user.User;
import users.user.UserManagement;

import java.io.BufferedReader;
import java.io.BufferedWriter;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static utils.CommonUtils.hasAtLeastOneAdministratorAccount;
import static validation.ValidationService.*;

public class UserRegistrationController {

    private static final DSLContext DSL_CONTEXT = DSLContextProvider.createDslContext();
    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void handleNewUserRegistration
            (BufferedWriter bufferedWriter, BufferedReader bufferedReader) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        LOGGER.info("\nChosen command - sign in");
        if (!hasAtLeastOneAdministratorAccount()) {
            clientRegistrationHandleAsAdmin(bufferedWriter, bufferedReader);
        } else {
            clientRegistrationHandleAsUser(bufferedWriter, bufferedReader);
        }
    }

    private void clientRegistrationHandleAsAdmin(BufferedWriter bufferedWriter, BufferedReader bufferedReader) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        LOGGER.info("No administrator account found. Registering the first administrator.");
        String noAdminMessage = "No admin account. You'll become an admin upon creating your account.";
        sendMessage(bufferedWriter, noAdminMessage);
        String adminLogin = getLogin(bufferedWriter, bufferedReader);
        String adminPassword = getPassword(bufferedWriter, bufferedReader);
        LOGGER.info("User entered correct password.");
        createAdminAndSaveToFile(adminLogin, adminPassword, bufferedWriter);
    }

    private void clientRegistrationHandleAsUser(BufferedWriter bufferedWriter, BufferedReader bufferedReader) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        LOGGER.info("Registering a new user account.");
        String userLogin = getLogin(bufferedWriter, bufferedReader);
        String userPassword = getPassword(bufferedWriter, bufferedReader);
        LOGGER.info("Admin entered correct password.");
        createUserAndSaveToFile(userLogin, userPassword, bufferedWriter);
    }

    private String getLogin(BufferedWriter bufferedWriter, BufferedReader bufferedReader) {
        String login;
        boolean doesTheLoginAlreadyExistInTheDatabase;
        String enterLoginLabel = "Enter login:" + System.lineSeparator();
        sendMessage(bufferedWriter, enterLoginLabel);
        do {
            login = returnTheAnswerAsString(bufferedReader).trim();
//            sprawdza czy itnieje w json
//            doesTheLoginAlreadyExistInTheDatabase = checkingIfALoginExistsInTheJsonFile(login);
            //metoda do database
            UserRepository userRepository = new UserRepository(DSL_CONTEXT);
            doesTheLoginAlreadyExistInTheDatabase = userRepository.checkingIfAUserLoginExists(login);
            handleExistingLogin(doesTheLoginAlreadyExistInTheDatabase, bufferedWriter, login);
        } while (doesTheLoginAlreadyExistInTheDatabase);
        return login;
    }

    private void handleExistingLogin(boolean doesTheLoginAlreadyExist, BufferedWriter bufferedWriter, String login) {
        isStringValid(login);
        String message;
        if (doesTheLoginAlreadyExist) {
            message = "User provided a login that already exists in the database - \"" + login + "\"";
        } else {
            message = "Login entered:     " + login;
        }
        sendMessage(bufferedWriter, doesTheLoginAlreadyExist + System.lineSeparator());
        LOGGER.info("{}", message);
    }

    private String getPassword(BufferedWriter bufferedWriter, BufferedReader bufferedReader) {
        String enterPasswordLabel = "Enter password:" + System.lineSeparator();
        sendMessage(bufferedWriter, enterPasswordLabel);
        return returnTheAnswerAsString(bufferedReader).trim();
    }

    private void createAdminAndSaveToFile(String login, String password, BufferedWriter bufferedWriter) {
        isStringValid(login);
        isStringValid(password);
        LOGGER.info("Admin \"{}\" has been created.", login.trim());
        AdminManagement adminManagement = new AdminManagement();
        Admin admin = adminManagement.createNewAdmin(login, password);
        LOGGER.info("Admin \"{}\" was added in the admins.json file.", admin.getLogin());
        String registrationConfirmed = "Registration was successful." + System.lineSeparator();
        sendMessage(bufferedWriter, registrationConfirmed);
    }

    private void createUserAndSaveToFile(String login, String password, BufferedWriter bufferedWriter) {
        isStringValid(login);
        isStringValid(password);
        LOGGER.info("User \"{}\" has been created.", login.trim());
        UserManagement userManagement = new UserManagement();
        User user = userManagement.createNewUser(login, password);
        LOGGER.info("User \"{}\" was added in the users.json file.", user.getLogin());
        String registrationConfirmed = "Registration was successful." + System.lineSeparator();
        sendMessage(bufferedWriter, registrationConfirmed);
    }

}
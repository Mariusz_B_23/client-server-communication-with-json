package server;

import commands.CommandsHandle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.account.logging.UserLoginManager;
import server.account.registration.UserRegistrationController;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.ServerSocket;
import java.time.Instant;

import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static utils.CommonUtils.hasAtLeastOneAdministratorAccount;
import static utils.CommonUtils.sendInitialMessagesToClient;
import static validation.ValidationService.*;

public class ServerCommunicationManager {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public void initiationOfCommunicationWithTheClient(ServerSocket serverSocket, BufferedWriter bufferedWriter,
                                                       BufferedReader bufferedReader, Instant serverLaunchTime) {
        isServerSocketValid(serverSocket);
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isServerLaunchTimeValid(serverLaunchTime);
        sendInitialMessagesToClient(bufferedWriter);
        String readCommandFromClient;
        CommandsHandle commandsHandle = new CommandsHandle();
        while (!serverSocket.isClosed()) {
            String adminListStatus = hasAtLeastOneAdministratorAccount() + System.lineSeparator();
            sendMessage(bufferedWriter, adminListStatus);
            readCommandFromClient = returnTheAnswerAsString(bufferedReader);
            if (readCommandFromClient.equals("stop")) {
                commandsHandle.processStopCommandForServerSocket(bufferedWriter, serverSocket);
            } else {
                switch (readCommandFromClient) {
                    case "1" -> {
                        UserRegistrationController userRegistrationController = new UserRegistrationController();
                        userRegistrationController.handleNewUserRegistration
                                (bufferedWriter, bufferedReader);
                    }
                    case "2" -> {
                        UserLoginManager userLoginManager = new UserLoginManager();
                        userLoginManager.performLoginProcess
                                (bufferedWriter, bufferedReader, serverLaunchTime);
                    }
                    default -> {
                        LOGGER.info("Chosen bad command - {} - is wrong.", readCommandFromClient);
                        commandsHandle.processWrongCommand(bufferedWriter);
                    }
                }
                sendInitialMessagesToClient(bufferedWriter);
            }
        }
    }

}
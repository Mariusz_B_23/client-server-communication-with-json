package app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static app.Client.startClientSocket;
import static app.Server.startServer;
import static utils.PropertiesUtils.getClientIP;
import static utils.PropertiesUtils.getServerPortNumber;

public class App {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    public static void main(String[] args) {

        int serverPortNumber = getServerPortNumber();
        String ipClient = getClientIP();

        try {
            new Thread(() -> startServer(serverPortNumber)).start();
            Thread.sleep(1000);
            LOGGER.info("Server started on port: {}. Now starting client.", serverPortNumber);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.error("Operation was interrupted: {}", e.getMessage(), e);
        }

        startClientSocket(ipClient, serverPortNumber);
        LOGGER.info("Starting client socket. Connecting to {}:{}", ipClient, serverPortNumber);
    }

}
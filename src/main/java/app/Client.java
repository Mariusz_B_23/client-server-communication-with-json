package app;

import client.ClientCommunicationManager;
import exceptions.CloseClientSocketException;
import exceptions.ConnectionWithServerException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

import static validation.ValidationService.*;

public class Client {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    private Client(){
    }

    public static void startClientSocket(String ipAddress, int portNumber) {
        isIpAddressValid(ipAddress);
        isPortNumberValid(portNumber);
        try (
                Socket clientSocket = new Socket(ipAddress, portNumber);
                BufferedWriter bufferedWriter =
                        new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
                BufferedReader bufferedReader =
                        new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                Scanner scanner = new Scanner(System.in)
        ) {
            initiationOfAConnectionBetweenClientAndServer
                    (clientSocket, bufferedWriter, bufferedReader, scanner);
        } catch (IOException e) {
            throw new ConnectionWithServerException("An error occurred while connecting to the server.", e);
        }
    }

    private static void initiationOfAConnectionBetweenClientAndServer
            (Socket clientSocket, BufferedWriter bufferedWriter, BufferedReader bufferedReader, Scanner scanner) {
        isClientSocketValid(clientSocket);
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isScannerValid(scanner);
        ClientCommunicationManager clientCommandHandler = new ClientCommunicationManager();
        LOGGER.info("The client is connected to the server.\n");
        while (!clientSocket.isClosed()) {
            clientCommandHandler
                    .initiationOfCommunicationWithTheServer(clientSocket, bufferedWriter, bufferedReader, scanner);
        }
    }

    public static void closeClientSocket(Socket clientSocket) {
        isClientSocketValid(clientSocket);
        try {
            clientSocket.close();
        } catch (IOException e) {
            throw new CloseClientSocketException("An error occurred while closing the client socket.", e);
        }
    }

}
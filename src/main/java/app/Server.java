package app;

import exceptions.CloseServerSocketException;
import exceptions.StartServerException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import server.ServerCommunicationManager;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Instant;

import static utils.CommonUtils.loadAdminsAndUsersFromJsonFilesToLists;
import static validation.ValidationService.*;

public class Server {

    private static final Logger LOGGER = LogManager.getLogger("cs_app");

    private Server() {
    }

    public static void startServer(int portNumber) {
        isPortNumberValid(portNumber);
        LOGGER.info("The server has been launched.\n");
        try (ServerSocket serverSocket = new ServerSocket(portNumber);
             Socket clientSocket = serverSocket.accept();
             BufferedWriter bufferedWriter =
                     new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
             BufferedReader bufferedReader =
                     new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))
        ) {
            final Instant serverLaunchTime = Instant.now();
            loadAdminsAndUsersFromJsonFilesToLists();
            initiationOfAConnectionBetweenServerAndClient
                    (serverSocket, bufferedWriter, bufferedReader, serverLaunchTime);
        } catch (IOException e) {
            throw new StartServerException("An error occurred during the server start.", e);
        }
    }

    private static void initiationOfAConnectionBetweenServerAndClient
            (ServerSocket serverSocket, BufferedWriter bufferedWriter,
             BufferedReader bufferedReader, Instant serverLaunchTime) {
        isServerSocketValid(serverSocket);
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isServerLaunchTimeValid(serverLaunchTime);
        ServerCommunicationManager serverCommandHandler = new ServerCommunicationManager();
        LOGGER.info("A new client has connected.\n");
        while (!serverSocket.isClosed()) {
            serverCommandHandler.initiationOfCommunicationWithTheClient
                    (serverSocket, bufferedWriter, bufferedReader, serverLaunchTime);
        }
    }

    public static void closeServerSocket(ServerSocket serverSocket) {
        isServerSocketValid(serverSocket);
        try {
            LOGGER.info("\nThe application has been closed.");
            serverSocket.close();
        } catch (IOException e) {
            throw new CloseServerSocketException("An error occurred while closing the server socket.", e);
        }
    }

}
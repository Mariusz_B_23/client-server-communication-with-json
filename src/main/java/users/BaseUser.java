package users;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import mailbox.MailBox;
import utils.HandlingRights;

@Data
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class BaseUser {

    protected String login;
    protected String password;
    protected boolean isAdmin;
    protected HandlingRights handlingRights;
    protected MailBox mailBox;


    public boolean getIsAdmin() {
        return isAdmin;
    }

    @Override
    public String toString() {
        return "Login: " + login + System.lineSeparator()
                + "------- MAILBOX --------" + System.lineSeparator()
                + mailBox.toString();
    }

}

package users.admin;


import database.DSLContextProvider;
import database.UserRepository;
import json.JsonReader;
import json.JsonWriter;
import lombok.Getter;
import mailbox.MailBox;
import org.jooq.DSLContext;

import java.util.ArrayList;
import java.util.List;

@Getter
public class AdminManagement {

    @Getter
    private static List<Admin> adminList = new ArrayList<>();

    private static final DSLContext DSL_CONTEXT = DSLContextProvider.createDslContext();


    public Admin createNewAdmin(String login, String password) {
        MailBox mailBox = new MailBox();
        Admin admin = new Admin(login, password, mailBox);
        getAdminList().add(admin);
        JsonWriter jsonWriter = new JsonWriter();
        jsonWriter.saveAdminListToJsonFile();
        UserRepository userRepository = new UserRepository(DSL_CONTEXT);
        userRepository.createUser(admin);
        return admin;
    }

    public static void readAllAdminsFromJsonFile() {
        JsonReader jsonReader = new JsonReader();
        adminList = jsonReader.readAdminListFromJsonFile();
    }

    public static boolean doesLoginExistAsAdmin(String adminLogin) {
        for (Admin admin : adminList) {
            if (admin.getLogin().equals(adminLogin)) {
                return true;
            }
        }
        return false;
    }

    public static boolean adminLoggingIsCorrect(String login, String password) {
        for (Admin admin : adminList) {
            if (admin.getLogin().equals(login) && admin.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    public Admin getAdminFromListByPasswordAndLogin(String userLogin, String userPassword) {
        return adminList.stream()
                .filter(admin -> admin.getLogin().equals(userLogin) && admin.getPassword().equals(userPassword))
                .findFirst()
                .orElse(null);
    }

    public static Admin getAdminFromListByLogin(String userLogin) {
        return adminList.stream()
                .filter(admin -> admin.getLogin().equals(userLogin))
                .findFirst()
                .orElse(null);
    }

    public static boolean isAdminPasswordValid(Admin admin, String password) {
        for (Admin storedAdmin : adminList) {
            if (storedAdmin.getLogin().equals(admin.getLogin()) && admin.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    public static void removeAdminFromList(Admin admin) {
        adminList.remove(admin);
    }

}
package users.admin;

import mailbox.MailBox;
import users.BaseUser;
import users.user.User;
import utils.HandlingRights;

import static utils.HandlingRights.ADMIN;


public class Admin extends BaseUser {


    public Admin() {
    }

    public Admin(String login, String password, boolean isAdmin, HandlingRights handlingRights, MailBox mailBox) {
        super(login, password, isAdmin, handlingRights, mailBox);
    }

    public Admin(String login, String password, MailBox mailBox) {
        super(login, password, true, ADMIN, mailBox);
    }

    public Admin(User user) {
        super(user.getLogin(), user.getPassword(), true, ADMIN, user.getMailBox());
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
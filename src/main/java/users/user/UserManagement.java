package users.user;


import database.DSLContextProvider;
import database.UserRepository;
import json.JsonReader;
import json.JsonWriter;
import lombok.Getter;
import mailbox.MailBox;
import message.Message;
import org.jooq.DSLContext;
import users.BaseUser;

import java.util.ArrayList;
import java.util.List;


public class UserManagement {

    @Getter
    private static List<User> userList = new ArrayList<>();
    private static final DSLContext DSL_CONTEXT = DSLContextProvider.createDslContext();

    public static void readAllUsersFromJsonFile() {
        JsonReader jsonReader = new JsonReader();
        userList = jsonReader.readUserListFromJsonFile();
    }

    public User createNewUser(String login, String password) {
        MailBox mailBox = new MailBox();
        User user = new User(login, password, mailBox);
        getUserList().add(user);
        JsonWriter jsonWriter = new JsonWriter();
        jsonWriter.saveUserListToJsonFile();
        UserRepository userRepository = new UserRepository(DSL_CONTEXT);
        userRepository.createUser(user);
        return user;
    }

    public User getUserFromList(String userLogin, String userPassword) {
        return userList.stream()
                .filter(user -> user.getLogin().equals(userLogin) && user.getPassword().equals(userPassword))
                .findFirst()
                .orElse(null);
    }

    public static User getUserFromListByLogin(String userLogin) {
        return userList.stream()
                .filter(user -> user.getLogin().equals(userLogin))
                .findFirst()
                .orElse(null);
    }

    public static boolean doesLoginExistAsUser(String userLogin) {
        for (User user : userList) {
            if (user.getLogin().equals(userLogin)) {
                return true;
            }
        }
        return false;
    }

    public static boolean userLoggingIsCorrect(String login, String password) {
        for (User user : userList) {
            if (user.getLogin().equals(login) && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isUserPasswordValid(User user, String password) {
        for (User storedUser : userList) {
            if (storedUser.getLogin().equals(user.getLogin()) && user.getPassword().equals(password)) {
                return true;
            }
        }
        return false;
    }

    public static void removeUserFromList(User user) {
        userList.remove(user);
    }



}
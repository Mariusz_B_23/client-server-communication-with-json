package users.user;

import mailbox.MailBox;
import users.BaseUser;
import users.admin.Admin;
import utils.HandlingRights;

import static utils.HandlingRights.USER;


public class User extends BaseUser {


    public User() {
    }

    public User(String login, String password, boolean isAdmin, HandlingRights handlingRights, MailBox mailBox) {
        super(login, password, isAdmin, handlingRights, mailBox);
    }

    public User(String login, String password, MailBox mailBox) {
        super(login, password, false, USER, mailBox);
    }

    public User(Admin admin) {
        super(admin.getLogin(), admin.getPassword(), false, USER, admin.getMailBox());
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
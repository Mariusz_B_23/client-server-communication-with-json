/*
 * This file is generated by jOOQ.
 */
package jooq.generate.tables.records;


import jooq.generate.enums.HandlingRights;
import jooq.generate.tables.Users;

import org.jooq.Record1;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UsersRecord extends UpdatableRecordImpl<UsersRecord> {

    private static final long serialVersionUID = 1L;

    /**
     * Setter for <code>public.users.id</code>.
     */
    public void setId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>public.users.id</code>.
     */
    public Integer getId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>public.users.login</code>.
     */
    public void setLogin(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>public.users.login</code>.
     */
    public String getLogin() {
        return (String) get(1);
    }

    /**
     * Setter for <code>public.users.password</code>.
     */
    public void setPassword(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>public.users.password</code>.
     */
    public String getPassword() {
        return (String) get(2);
    }

    /**
     * Setter for <code>public.users.handling_rights</code>.
     */
    public void setHandlingRights(HandlingRights value) {
        set(3, value);
    }

    /**
     * Getter for <code>public.users.handling_rights</code>.
     */
    public HandlingRights getHandlingRights() {
        return (HandlingRights) get(3);
    }

    /**
     * Setter for <code>public.users.isadmin</code>.
     */
    public void setIsadmin(Boolean value) {
        set(4, value);
    }

    /**
     * Getter for <code>public.users.isadmin</code>.
     */
    public Boolean getIsadmin() {
        return (Boolean) get(4);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached UsersRecord
     */
    public UsersRecord() {
        super(Users.USERS);
    }

    /**
     * Create a detached, initialised UsersRecord
     */
    public UsersRecord(Integer id, String login, String password, HandlingRights handlingRights, Boolean isadmin) {
        super(Users.USERS);

        setId(id);
        setLogin(login);
        setPassword(password);
        setHandlingRights(handlingRights);
        setIsadmin(isadmin);
        resetChangedOnNotNull();
    }
}

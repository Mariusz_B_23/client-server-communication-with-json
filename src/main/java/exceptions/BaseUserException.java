package exceptions;

public class BaseUserException extends RuntimeException {

    public BaseUserException(String message) {
        super(message);
    }

}
package exceptions;

public class ResponseFromTheServerException extends RuntimeException {

    public ResponseFromTheServerException(String message, Throwable cause) {
        super(message, cause);
    }

}
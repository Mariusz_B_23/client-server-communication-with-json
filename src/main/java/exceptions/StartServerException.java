package exceptions;

public class StartServerException extends RuntimeException {

    public StartServerException(String message, Throwable cause) {
        super(message, cause);
    }

}
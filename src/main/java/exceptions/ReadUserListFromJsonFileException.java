package exceptions;

public class ReadUserListFromJsonFileException extends RuntimeException {


    public ReadUserListFromJsonFileException(String message, Throwable cause) {
        super(message, cause);
    }

}
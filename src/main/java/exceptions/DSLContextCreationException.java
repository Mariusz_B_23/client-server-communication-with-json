package exceptions;

public class DSLContextCreationException extends RuntimeException {

    public DSLContextCreationException(String message, Throwable cause) {
        super(message, cause);
    }

}
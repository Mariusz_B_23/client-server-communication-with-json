package exceptions;

public class IpAddressException extends RuntimeException {

    public IpAddressException(String message) {
        super(message);
    }

}
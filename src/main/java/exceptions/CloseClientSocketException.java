package exceptions;

public class CloseClientSocketException extends RuntimeException {

    public CloseClientSocketException(String message) {
        super(message);
    }

    public CloseClientSocketException(String message, Throwable cause) {
        super(message, cause);
    }

}
package exceptions;

public class JsonFileOperationException extends RuntimeException {

    public JsonFileOperationException(String message, Throwable cause) {
        super(message, cause);
    }

    public JsonFileOperationException(String message) {
        super(message);
    }

}
package exceptions;

public class PortNumberException extends RuntimeException {

    public PortNumberException(String message) {
        super(message);
    }

}
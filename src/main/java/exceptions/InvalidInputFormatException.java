package exceptions;

public class InvalidInputFormatException extends RuntimeException {

    public InvalidInputFormatException(String message) {
        super(message);
    }

}
package exceptions;

public class StringException extends RuntimeException {

    public StringException(String message) {
        super(message);
    }

}
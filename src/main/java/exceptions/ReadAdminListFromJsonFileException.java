package exceptions;

public class ReadAdminListFromJsonFileException extends RuntimeException {


    public ReadAdminListFromJsonFileException(String message, Throwable cause) {
        super(message, cause);
    }

}
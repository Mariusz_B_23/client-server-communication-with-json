package exceptions;

public class BufferedWriterException extends RuntimeException {

    public BufferedWriterException(String message) {
        super(message);
    }

    public BufferedWriterException(String message, Throwable cause) {
        super(message, cause);
    }

}
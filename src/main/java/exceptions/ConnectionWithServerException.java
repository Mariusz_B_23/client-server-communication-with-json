package exceptions;

public class ConnectionWithServerException extends RuntimeException {

    public ConnectionWithServerException(String message, Throwable cause) {
        super(message, cause);
    }

}
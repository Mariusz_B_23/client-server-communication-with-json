package exceptions;

public class CodeGenerationException extends RuntimeException {

    public CodeGenerationException(String message, Throwable cause) {
        super(message, cause);
    }

}
package exceptions;

public class CloseServerSocketException extends RuntimeException {

    public CloseServerSocketException(String message) {
        super(message);
    }

    public CloseServerSocketException(String message, Throwable cause) {
        super(message, cause);
    }

}
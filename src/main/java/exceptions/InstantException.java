package exceptions;

public class InstantException extends RuntimeException {

    public InstantException(String message) {
        super(message);
    }

}
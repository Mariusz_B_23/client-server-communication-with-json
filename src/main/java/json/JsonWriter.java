package json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import exceptions.JsonFileOperationException;
import exceptions.JsonProcessingRuntimeException;
import message.Message;
import users.BaseUser;
import users.admin.Admin;
import users.admin.AdminManagement;
import users.user.User;
import users.user.UserManagement;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;

import static utils.PropertiesUtils.getAdminsJsonFilepath;
import static utils.PropertiesUtils.getUsersJsonFilepath;
import static validation.ValidationService.*;

public class JsonWriter {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final File USERS_LIST_AS_JSON = new File(getUsersJsonFilepath());
    private static final File ADMINS_LIST_AS_JSON = new File(getAdminsJsonFilepath());


    public String createJsonStringCommand(String headline, String message) {
        isStringValid(headline);
        isStringValid(message);
        try {
            ObjectNode objectNode = OBJECT_MAPPER.createObjectNode();
            objectNode.put(headline, message);
            return OBJECT_MAPPER.writeValueAsString(objectNode);
        } catch (JsonProcessingException e) {
            throw new JsonProcessingRuntimeException("Error when saving json as String.", e);
        }
    }

    public void saveUserListToJsonFile() {
        try {
            OBJECT_MAPPER.registerModule(new JavaTimeModule());
            OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
            OBJECT_MAPPER.writeValue(USERS_LIST_AS_JSON, UserManagement.getUserList());
        } catch (IOException e) {
            throw new JsonFileOperationException("Failed to save user list to JSON file.", e);
        } finally {
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    public void saveAdminListToJsonFile() {
        try {
            OBJECT_MAPPER.registerModule(new JavaTimeModule());
            OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
            OBJECT_MAPPER.writeValue(new File(getAdminsJsonFilepath()), AdminManagement.getAdminList());
        } catch (IOException e) {
            throw new JsonFileOperationException("Failed to save admin list to JSON file.", e);
        } finally {
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    public void changePasswordLineInUsersJsonFile(BaseUser baseUser, String newPassword) {
        isBaseUserValid(baseUser);
        isStringValid(newPassword);

        try {
            OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
            JsonNode usersAsNode = OBJECT_MAPPER.readTree(USERS_LIST_AS_JSON);
            for (JsonNode userJsonNode : usersAsNode) {
                String userLogin = userJsonNode.get("login").asText();

                if (userLogin.equals(baseUser.getLogin())) {
                    ((ObjectNode) userJsonNode).put("password", newPassword);
                    break;
                }
            }
            OBJECT_MAPPER.writeValue(USERS_LIST_AS_JSON, usersAsNode);
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        } catch (IOException e) {
            throw new JsonFileOperationException("Failed to change password in users JSON file.", e);
        } finally {
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    public void changePasswordLineInAdminsJsonFile(BaseUser baseUser, String newPassword) {
        isBaseUserValid(baseUser);
        isStringValid(newPassword);

        try {
            OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
            JsonNode adminsAsNode = OBJECT_MAPPER.readTree(ADMINS_LIST_AS_JSON);
            for (JsonNode userJsonNode : adminsAsNode) {
                String adminLogin = userJsonNode.get("login").asText();

                if (adminLogin.equals(baseUser.getLogin())) {
                    ((ObjectNode) userJsonNode).put("password", newPassword);
                    break;
                }
            }
            OBJECT_MAPPER.writeValue(ADMINS_LIST_AS_JSON, adminsAsNode);
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        } catch (IOException e) {
            throw new JsonFileOperationException("Failed to change password in admins JSON file.", e);
        } finally {
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    public void addMessageToUserUnreadBoxInJsonFile(String nicknameProvidedByUser, Message newMessage) {
        isStringValid(nicknameProvidedByUser);
        isMessageValid(newMessage);
        try {
            OBJECT_MAPPER.registerModule(new JavaTimeModule());
            OBJECT_MAPPER.setDateFormat(new SimpleDateFormat("dd.MM.yyyy  HH:mm:ss"));
            OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
            JsonNode usersAsNode = OBJECT_MAPPER.readTree(USERS_LIST_AS_JSON);
            for (JsonNode userJsonNode : usersAsNode) {
                String userLogin = userJsonNode.get("login").asText();
                if (userLogin.equals(nicknameProvidedByUser)) {
                    ArrayNode unreadMessagesArray = (ArrayNode) userJsonNode
                            .get("mailBox")
                            .get("unreadBox")
                            .get("unreadMessages");
                    ObjectNode newMessageNode = OBJECT_MAPPER.createObjectNode();
                    newMessageNode.put("sender", newMessage.getSender());
                    newMessageNode.put("recipient", newMessage.getRecipient());
                    newMessageNode.put("receivedDate", String.valueOf(newMessage.getReceivedDate()));
                    newMessageNode.put("isRead", newMessage.getIsRead());
                    newMessageNode.put("content", newMessage.getContent());
                    unreadMessagesArray.add(newMessageNode);
                    break;
                }
            }
            OBJECT_MAPPER.writeValue(USERS_LIST_AS_JSON, usersAsNode);
        } catch (IOException e) {
            throw new JsonFileOperationException("Failed to add message to user unread box in json file.", e);
        } finally {
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    public void addMessageToAdminUnreadBoxInJsonFile(String nicknameProvidedByUser, Message newMessage) {
        try {
            OBJECT_MAPPER.registerModule(new JavaTimeModule());
            OBJECT_MAPPER.setDateFormat(new SimpleDateFormat("dd.MM.yyyy  HH:mm:ss"));
            OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
            JsonNode adminsAsNode = OBJECT_MAPPER.readTree(ADMINS_LIST_AS_JSON);
            for (JsonNode adminJsonNode : adminsAsNode) {
                String adminLogin = adminJsonNode.get("login").asText();

                if (adminLogin.equals(nicknameProvidedByUser)) {
                    ArrayNode unreadMessagesArray = (ArrayNode) adminJsonNode
                            .get("mailBox")
                            .get("unreadBox")
                            .get("unreadMessages");
                    ObjectNode newMessageNode = OBJECT_MAPPER.createObjectNode();
                    newMessageNode.put("sender", newMessage.getSender());
                    newMessageNode.put("recipient", newMessage.getRecipient());
                    newMessageNode.put("receivedDate", String.valueOf(newMessage.getReceivedDate()));
                    newMessageNode.put("isRead", newMessage.getIsRead());
                    newMessageNode.put("content", newMessage.getContent());
                    unreadMessagesArray.add(newMessageNode);
                    break;
                }
            }
            OBJECT_MAPPER.writeValue(ADMINS_LIST_AS_JSON, adminsAsNode);
        } catch (IOException e) {
            throw new JsonFileOperationException("Failed to add message to admin unread box in json file.", e);
        } finally {
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    public void removeMessageFromUserInBoxInJsonFile(BaseUser baseUser, int index) {
        try {
            OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
            JsonNode usersAsNode = OBJECT_MAPPER.readTree(USERS_LIST_AS_JSON);
            int indexUpdatedToListNumbering = (index - 1);
            for (JsonNode userJsonNode : usersAsNode) {
                String userLogin = userJsonNode.get("login").asText();
                if (userLogin.equals(baseUser.getLogin())) {
                    ArrayNode readMessagesArray = (ArrayNode) userJsonNode
                            .get("mailBox")
                            .get("inbox")
                            .get("readMessages");
                    if (indexUpdatedToListNumbering >= 0 && indexUpdatedToListNumbering < readMessagesArray.size()) {
                        readMessagesArray.remove(indexUpdatedToListNumbering);
                    }
                    break;
                }
            }
            OBJECT_MAPPER.writeValue(USERS_LIST_AS_JSON, usersAsNode);
        } catch (IOException e) {
            throw new JsonFileOperationException("Failed to remove message from user inbox in json file.", e);
        } finally {
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    public void removeMessageFromAdminInBoxInJsonFile(BaseUser baseUser, int index) {
        try {
            OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
            JsonNode adminsAsNode = OBJECT_MAPPER.readTree(ADMINS_LIST_AS_JSON);
            int indexUpdatedToListNumbering = (index - 1);
            for (JsonNode adminJsonNode : adminsAsNode) {
                String adminLogin = adminJsonNode.get("login").asText();
                if (adminLogin.equals(baseUser.getLogin())) {
                    ArrayNode readMessagesArray = (ArrayNode) adminJsonNode
                            .get("mailBox")
                            .get("inbox")
                            .get("readMessages");
                    if (indexUpdatedToListNumbering >= 0 && indexUpdatedToListNumbering < readMessagesArray.size()) {
                        readMessagesArray.remove(indexUpdatedToListNumbering);
                    }
                    break;
                }
            }
            OBJECT_MAPPER.writeValue(ADMINS_LIST_AS_JSON, adminsAsNode);
        } catch (IOException e) {
            throw new JsonFileOperationException("Failed to remove message from admin inbox in json file.", e);
        } finally {
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    public static void removeUserFromJson(User user) {
        try {
            OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
            JsonNode jsonNode = OBJECT_MAPPER.readTree(USERS_LIST_AS_JSON);
            if (jsonNode.isArray()) {
                for (int i = 0; i < jsonNode.size(); i++) {
                    JsonNode userNode = jsonNode.get(i);
                    if (userNode.get("login").asText().equals(user.getLogin())) {
                        ((ArrayNode) jsonNode).remove(i);
                        break;
                    }
                }
                OBJECT_MAPPER.writeValue(new File(getUsersJsonFilepath()), jsonNode);
            }
        } catch (IOException e) {
            throw new JsonFileOperationException("Failed to remove user  from json file.", e);
        } finally {
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    public static void removeAdminFromJson(Admin admin) {
        try {
            OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
            JsonNode jsonNode = OBJECT_MAPPER.readTree(ADMINS_LIST_AS_JSON);
            if (jsonNode.isArray()) {
                for (int i = 0; i < jsonNode.size(); i++) {
                    JsonNode adminNode = jsonNode.get(i);
                    if (adminNode.get("login").asText().equals(admin.getLogin())) {
                        ((ArrayNode) jsonNode).remove(i);
                        break;
                    }
                }
                OBJECT_MAPPER.writeValue(new File(getAdminsJsonFilepath()), jsonNode);
            }
        } catch (IOException e) {
            throw new JsonFileOperationException("Failed to remove admin  from json file.", e);
        } finally {
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    public static void addAdminToJson(Admin admin) {
        try {
            OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
            JsonNode jsonNode = OBJECT_MAPPER.readTree(ADMINS_LIST_AS_JSON);
            JsonNode newAdminNode = OBJECT_MAPPER.valueToTree(admin);
            ((ArrayNode) jsonNode).add(newAdminNode);
            OBJECT_MAPPER.writeValue(ADMINS_LIST_AS_JSON, jsonNode);
        } catch (IOException e) {
            throw new JsonFileOperationException("Failed to add admin  to json file.", e);
        } finally {
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        }
    }

    public static void addUserToJson(User user) {
        try {
            OBJECT_MAPPER.enable(SerializationFeature.INDENT_OUTPUT);
            JsonNode jsonNode = OBJECT_MAPPER.readTree(USERS_LIST_AS_JSON);
            JsonNode newUserNode = OBJECT_MAPPER.valueToTree(user);
            ((ArrayNode) jsonNode).add(newUserNode);
            OBJECT_MAPPER.writeValue(USERS_LIST_AS_JSON, jsonNode);
        } catch (IOException e) {
            throw new JsonFileOperationException("Failed to add admin  to json file.", e);
        } finally {
            OBJECT_MAPPER.disable(SerializationFeature.INDENT_OUTPUT);
        }
    }

}
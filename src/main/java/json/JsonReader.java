package json;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import exceptions.ReadAdminListFromJsonFileException;
import exceptions.ReadUserListFromJsonFileException;
import users.admin.Admin;
import users.user.User;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static utils.PropertiesUtils.getAdminsJsonFilepath;
import static utils.PropertiesUtils.getUsersJsonFilepath;

public class JsonReader {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().registerModule(new JavaTimeModule());


    public List<Admin> readAdminListFromJsonFile() {
        List<Admin> adminList;
        try {
            File file = new File(getAdminsJsonFilepath());
            if (file.length() == 0) {
                return new ArrayList<>();
            } else {
                adminList = OBJECT_MAPPER.readValue(new File(getAdminsJsonFilepath()), new TypeReference<>() {
                });
            }
        } catch (IOException e) {
            throw new ReadAdminListFromJsonFileException
                    ("An error occurred while reading admin list from JSON file.", e);
        }
        return adminList;
    }

    public List<User> readUserListFromJsonFile() {
        List<User> userList;
        try {
            File file = new File(getUsersJsonFilepath());
            if (file.length() == 0) {
                return new ArrayList<>();
            } else {
                userList = OBJECT_MAPPER.readValue(new File(getUsersJsonFilepath()), new TypeReference<>() {
                });
            }
        } catch (IOException e) {
            throw new ReadUserListFromJsonFileException
                    ("An error occurred while reading user list from JSON file.", e);
        }
        return userList;
    }

}
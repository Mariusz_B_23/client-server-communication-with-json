package validation;

import exceptions.*;
import message.Message;
import users.BaseUser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Instant;
import java.util.Properties;
import java.util.Scanner;

public class ValidationService {

    private ValidationService() {
    }

    public static void isIpAddressValid(String ipAddress) {
        if (ipAddress == null || ipAddress.isEmpty()) {
            throw new IpAddressException("IP address is null or empty.");
        }
        String regexIPv4 =
                "^((\\d{1,2}|1\\d{2}|2[0-4]\\d|25[0-5])\\.){3}(\\d{1,2}|1\\d{2}|2[0-4]\\d|25[0-5])$";
        String regexIPv6 =
                "^([0-9a-fA-F]{1,4}:){7}([0-9a-fA-F]{1,4})$";
        if (!ipAddress.equals("localhost") && !ipAddress.matches(regexIPv4) && !ipAddress.matches(regexIPv6)) {
            throw new IpAddressException("IP address is not valid: " + ipAddress);
        }
    }

    public static void isPortNumberValid(int portNumber) {
        if (portNumber <= 0 || portNumber > 65535) {
            throw new PortNumberException("Invalid port number: " + portNumber);
        }
    }

    public static void isClientSocketValid(Socket clientSocket) {
        if (clientSocket == null) {
            throw new IllegalArgumentException("Client socket cannot be null.");
        }
        if (!clientSocket.isConnected()) {
            throw new CloseClientSocketException("Client socket is not connected.");
        }
        if (clientSocket.isClosed()) {
            throw new CloseClientSocketException("Client socket is closed.");
        }
    }

    public static void isServerSocketValid(ServerSocket serverSocket) {
        if (serverSocket == null) {
            throw new IllegalArgumentException("Server socket cannot be null.");
        }
        if (serverSocket.isClosed()) {
            throw new CloseServerSocketException("Server socket is closed.");
        }
    }

    public static void isBufferedReaderValid(BufferedReader bufferedReader)
            throws BufferedReaderException {
        if (bufferedReader == null) {
            throw new BufferedReaderException("Invalid buffered reader.");
        }
    }

    public static void isBufferedWriterValid(BufferedWriter bufferedWriter)
            throws BufferedWriterException {
        if (bufferedWriter == null) {
            throw new BufferedWriterException("Invalid buffered writer.");
        }
    }

    public static void isScannerValid(Scanner scanner) {
        if (scanner == null) {
            throw new InvalidInputFormatException("Invalid scanner.");
        }
    }

    public static void isServerLaunchTimeValid(Instant serverLaunchTime) {
        if (serverLaunchTime == null) {
            throw new InstantException("Server start time is null.");
        }
        if (serverLaunchTime.isAfter(Instant.now())) {
            throw new InstantException("Server start time is in the future: " + serverLaunchTime);
        }
    }

    public static void isStringValid(String expression) {
        if (expression == null || expression.isEmpty()) {
            throw new StringException("String expression is null or empty: " + expression);
        }
    }

    public static void isPropertiesValid(Properties properties) {
        if (properties.isEmpty()) {
            throw new PropertiesException("Properties is empty.");
        }
    }

    public static void isBaseUserValid(BaseUser baseUser) {
        if (baseUser == null) {
            throw new BaseUserException("User or admin is null.");
        }
    }

    public static void isMessageValid(Message message) {
        if (message == null) {
            throw new MessageException("Message is null");
        }
    }

}
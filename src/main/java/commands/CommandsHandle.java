package commands;

import json.JsonWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import users.BaseUser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Duration;
import java.time.Instant;

import static app.Client.closeClientSocket;
import static app.Server.closeServerSocket;
import static utils.BufferedReaderUtils.returnTheAnswerAsString;
import static utils.BufferedWriterUtils.sendMessage;
import static utils.PropertiesUtils.getCreationServerDate;
import static utils.PropertiesUtils.getServerVersion;
import static validation.ValidationService.*;

public class CommandsHandle {

    private final JsonWriter jsonWriter = new JsonWriter();
    private static final Logger LOGGER = LogManager.getLogger("cs_app");


    public void processStopCommandForServerSocket(BufferedWriter bufferedWriter, ServerSocket serverSocket) {
        isBufferedWriterValid(bufferedWriter);
        isServerSocketValid(serverSocket);
        LOGGER.info("\nChosen command - stop");
        String stopCommandMessageToJsonAsString = createStopCommandMessage() + System.lineSeparator();
        sendMessage(bufferedWriter, stopCommandMessageToJsonAsString);
        closeServerSocket(serverSocket);
    }

    public String createStopCommandMessage() {
        String stopHeadline = "stop";
        String stopCommandMessage = "Stopping the operation of the server.";
        return jsonWriter.createJsonStringCommand(stopHeadline, stopCommandMessage);
    }

    public void processStopCommandForClientSocket(BufferedReader bufferedReader, Socket clientSocket) {
        isBufferedReaderValid(bufferedReader);
        isClientSocketValid(clientSocket);
        String serverResponse = returnTheAnswerAsString(bufferedReader);
        LOGGER.info("\n{}", serverResponse);

        closeClientSocket(clientSocket);
    }

    public void logoutHandling(BaseUser baseUser, BufferedWriter bufferedWriter) {
        isBaseUserValid(baseUser);
        isBufferedWriterValid(bufferedWriter);
        LOGGER.info("\nChosen command - log out");
        processLogoutCommand(bufferedWriter, baseUser);
    }

    private void processLogoutCommand(BufferedWriter bufferedWriter, BaseUser baseUser) {
        isBufferedWriterValid(bufferedWriter);
        isBaseUserValid(baseUser);
        String outHeadline = "out";
        String outMessage = String.format("%s %s successfully and safely, logged out.",
                baseUser.getHandlingRights().toString().toLowerCase(), baseUser.getLogin());
        String outMessageToJsonAsString =
                jsonWriter.createJsonStringCommand(outHeadline, outMessage);
        sendMessage(bufferedWriter, outMessageToJsonAsString + System.lineSeparator());
        LOGGER.info("\"{}\" has successfully and safely logged out.", baseUser.getLogin());

    }

    public void processWrongCommand(BufferedWriter bufferedWriter) {
        isBufferedWriterValid(bufferedWriter);
        String wrongCommandMessageToJsonAsString = createWrongCommandMessage() + System.lineSeparator();
        sendMessage(bufferedWriter, wrongCommandMessageToJsonAsString);
    }

    public String createWrongCommandMessage() {
        String wrongMessageHeadline = "wrongCommand";
        String wrongCommandMessage = "Incorrect command.";
        return jsonWriter.createJsonStringCommand(wrongMessageHeadline, wrongCommandMessage);
    }

    public void handleHelperCommandsMenu(BufferedWriter bufferedWriter, BufferedReader bufferedReader,
                                         Instant serverLaunchTime) {
        isBufferedWriterValid(bufferedWriter);
        isBufferedReaderValid(bufferedReader);
        isServerLaunchTimeValid(serverLaunchTime);
        boolean isSelectedBack;
        do {
            String message = "List of available commands:" + System.lineSeparator()
                    + "- uptime" + System.lineSeparator()
                    + "- info" + System.lineSeparator()
                    + "- back" + System.lineSeparator()
                    + "If you need help, use the command" + System.lineSeparator()
                    + "- help -> returns the list of available commands with their brief description."
                    + System.lineSeparator()
                    + "Enter command:" + System.lineSeparator();
            sendMessage(bufferedWriter, message);
            String respond = returnTheAnswerAsString(bufferedReader);
            isSelectedBack = respond.equals("back");
            processOtherAvailableCommands(bufferedWriter, respond, serverLaunchTime);
        } while (!isSelectedBack);
    }

    private void processOtherAvailableCommands
            (BufferedWriter bufferedWriter, String userRespond, Instant serverLaunchTime) {
        isBufferedWriterValid(bufferedWriter);
        isStringValid(userRespond);
        isServerLaunchTimeValid(serverLaunchTime);
        LOGGER.info("\nChosen command - {}}", userRespond);
        switch (userRespond) {
            case "uptime" -> processUptimeCommand(bufferedWriter, serverLaunchTime);
            case "info" -> processServerInfoCommand(bufferedWriter);
            case "help" -> processHelpCommand(bufferedWriter);
            case "back" -> processBackCommand(bufferedWriter);
            default -> processWrongCommand(bufferedWriter);
        }
    }

    private void processUptimeCommand(BufferedWriter bufferedWriter, Instant startOfTheServer) {
        isBufferedWriterValid(bufferedWriter);
        isServerLaunchTimeValid(startOfTheServer);
        String uptimeHeadline = "uptime";
        String uptimeMessage = getServerUptimeString(startOfTheServer);
        String uptimeMessageToJsonAsString = jsonWriter.createJsonStringCommand(uptimeHeadline, uptimeMessage);
        sendMessage(bufferedWriter, uptimeMessageToJsonAsString + System.lineSeparator());
    }

    public String getServerUptimeString(Instant serverStartTime) {
        isServerLaunchTimeValid(serverStartTime);
        Instant serverEndTime = Instant.now();
        Duration duration = Duration.between(serverStartTime, serverEndTime);
        long hours = duration.toHours();
        long minutes = duration.toMinutesPart();
        long seconds = duration.toSecondsPart();
        return String.format("Server operating time is: %d hours, %d minutes, %d seconds.",
                hours, minutes, seconds);
    }

    private void processServerInfoCommand(BufferedWriter bufferedWriter) {
        isBufferedWriterValid(bufferedWriter);
        String serverInfoHeadline = "serverInfo";
        String serverInfoMessage = getServerInfo();
        String serverInfoMessageToJsonAsString =
                jsonWriter.createJsonStringCommand(serverInfoHeadline, serverInfoMessage);
        sendMessage(bufferedWriter, serverInfoMessageToJsonAsString + System.lineSeparator());
    }

    private String getServerInfo() {
        String serverVersion = getServerVersion();
        String creationServerDate = getCreationServerDate();
        return "Server version: " + serverVersion
                + ". Server creation date: " + creationServerDate;
    }

    private void processHelpCommand(BufferedWriter bufferedWriter) {
        isBufferedWriterValid(bufferedWriter);
        String helpHeadline = "help";
        String helpMessage = getHelpCommandString();
        String helpMessageToJsonAsString = jsonWriter.createJsonStringCommand(helpHeadline, helpMessage);
        sendMessage(bufferedWriter, helpMessageToJsonAsString + System.lineSeparator());
    }

    public void processBackCommand(BufferedWriter bufferedWriter) {
        isBufferedWriterValid(bufferedWriter);
        String backHeadline = "back";
        String backMessage = "Back to main menu.";
        String backMessageToJsonAsString = jsonWriter.createJsonStringCommand(backHeadline, backMessage);
        sendMessage(bufferedWriter, backMessageToJsonAsString + System.lineSeparator());
    }

    private String getHelpCommandString() {
        return "Command description: " +
                "uptime -> returns the server lifetime, " +
                "info -> returns server version number and creation date, " +
                "back -> back to menu";
    }

}